package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
)

type instruction struct {
	s1 int
	s2 int
}

type instructions []instruction

type requirements map[int][]int

func getInstructions(filename string) instructions {
	result := instructions{}
	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		var s1, s2 string
		log.Println(scanner.Text())
		_, err := fmt.Sscanf(scanner.Text(), "Step %v must be finished before step %v can begin.", &s1, &s2)
		if err != nil {
			log.Fatal(err)
		}
		result = append(result, instruction{s1: int(s1[0]), s2: int(s2[0])})
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return result
}

func getRequirements(insts instructions) requirements {
	req := requirements{}
	for i := 0; i < len(insts); i++ {
		ins := insts[i]
		if req[ins.s2] == nil {
			req[ins.s2] = []int{ins.s1}
			continue
		}
		req[ins.s2] = append(req[ins.s2], ins.s1)
	}
	return req
}

func getIndependentSteps(req requirements) []int {
	stepsWithDeps := map[int]bool{}
	allSteps := map[int]bool{}
	for k, values := range req {
		stepsWithDeps[k] = true
		allSteps[k] = true
		for _, v := range values {
			allSteps[v] = true
		}
	}

	indepSteps := []int{}
	for k := range allSteps {
		if !stepsWithDeps[k] {
			indepSteps = append(indepSteps, k)
		}
	}

	sort.Ints(indepSteps)
	return indepSteps
}

func stepInList(step int, list []int) bool {
	for _, i := range list {
		if i == step {
			return true
		}
	}
	return false
}

func stepInPool(step int, pool []item) bool {
	for _, i := range pool {
		if i.step == step {
			return true
		}
	}
	return false
}

func deleteFromList(step int, list []int) []int {
	newList := []int{}
	for _, i := range list {
		if i == step {
			continue
		}
		newList = append(newList, i)
	}
	return newList
}

func getAvailableSteps(req requirements, woDepsSteps []int) []int {
	availableSteps := []int{}
	for k, deps := range req {
		if stepInList(k, woDepsSteps) {
			continue
		}

		allDepsDone := true
		for _, v := range deps {
			if !stepInList(v, woDepsSteps) {
				allDepsDone = false
				break
			}
		}

		if !allDepsDone {
			continue
		}

		availableSteps = append(availableSteps, k)
	}

	return availableSteps
}

func getAvailableStepsPool(req requirements, completed []int, pool []item) []int {
	availableSteps := []int{}
	for k, deps := range req {
		if stepInList(k, completed) {
			continue
		}

		if stepInPool(k, pool) {
			continue
		}

		allDepsDone := true
		for _, v := range deps {
			if !stepInList(v, completed) {
				allDepsDone = false
				break
			}
		}

		if !allDepsDone {
			continue
		}

		availableSteps = append(availableSteps, k)
	}

	return availableSteps
}

func followSteps(req requirements, indepSteps []int) {
	availableSteps := []int{}
	selectedSteps := []int{indepSteps[0]}
	indepSteps = indepSteps[1:]
	for {
		availableSteps = getAvailableSteps(req, selectedSteps)
		availableSteps = append(availableSteps, indepSteps...)
		//log.Printf("availableSteps %v\n", availableSteps)
		if len(availableSteps) == 0 {
			break
		}

		sort.Ints(availableSteps)
		selectedSteps = append(selectedSteps, availableSteps[0])
		//log.Printf("selectedSteps %v\n", selectedSteps)
		if stepInList(availableSteps[0], indepSteps) {
			indepSteps = indepSteps[1:]
			//log.Printf("indepSteps %v\n", indepSteps)
		}
	}

	res := ""
	for i := 0; i < len(selectedSteps); i++ {
		res += string(selectedSteps[i])
	}
	log.Printf("%v", res)
}

//const workers = 2
//const timeAddition = 0
const workers = 5
const timeAddition = 60
const itoaBase = 64

type item struct {
	step int
	ttl  int
}

func doIterationPool(pool []item) ([]int, []item) {
	completed := []int{}
	newPool := []item{}
	for _, i := range pool {
		if (i.ttl - 1) == 0 {
			completed = append(completed, i.step)
		} else {
			newPool = append(newPool, item{step: i.step, ttl: i.ttl - 1})
		}
	}
	return completed, newPool
}
func followStepsWithWorkers(req requirements, indepSteps []int) {
	availableSteps := []int{}
	completedSteps := []int{}
	completedIteration := []int{}
	iteration := -1
	pool := []item{}
	for x := 0; x < 1000000; x++ {
		iteration++
		completedIteration, pool = doIterationPool(pool)
		//log.Printf("pool after iteration %v\n", pool)
		completedSteps = append(completedSteps, completedIteration...)
		//log.Printf("completedSteps %v\n", completedSteps)

		availableSteps = getAvailableStepsPool(req, completedSteps, pool)
		availableSteps = append(availableSteps, indepSteps...)
		//log.Printf("availableSteps %v\n", availableSteps)
		if len(availableSteps) == 0 && len(pool) == 0 {
			break
		}

		sort.Ints(availableSteps)
		for {
			if len(availableSteps) == 0 {
				break
			}
			if len(pool) >= workers {
				break
			}
			pool = append(pool,
				item{step: availableSteps[0], ttl: availableSteps[0] - itoaBase + timeAddition})
			//log.Printf("pool added %v\n", pool)
			if stepInList(availableSteps[0], indepSteps) {
				indepSteps = deleteFromList(availableSteps[0], indepSteps)
				//log.Printf("indepSteps %v\n", indepSteps)
			}

			availableSteps = availableSteps[1:]
		}
		//log.Printf("pool %v\n", pool)
	}

	res := ""
	for i := 0; i < len(completedSteps); i++ {
		res += string(completedSteps[i])
	}
	log.Printf("%v", res)
	log.Printf("time: %d", iteration)
}

func main() {
	//CABDFE
	//instructions := getInstructions("input_example.txt")
	instructions := getInstructions("input.txt")
	log.Printf("instructions: %v", instructions)

	req := getRequirements(instructions)
	log.Printf("getRequirements: %v", req)
	indepSteps := getIndependentSteps(req)
	log.Printf("getIndependentSteps: %v", indepSteps)

	// Part1
	followSteps(req, indepSteps)
	// EBICGKQOVMYZJAWRDPXFSUTNLH

	//part2
	//test: CABFDE
	// EIVZBCGYJKAQWORMDPXFSUTNLH  - 906
	followStepsWithWorkers(req, indepSteps)
}
