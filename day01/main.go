package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
)

func getFrequencyChanges() []int64 {
	frequencyChanges := []int64{}

	f, err := os.Open("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		txt := scanner.Text()
		i, err := strconv.ParseInt(txt, 10, 64)
		if err != nil {
			log.Fatal(err)
		}
		frequencyChanges = append(frequencyChanges, i)
		log.Printf("txt: %v, int: %v", txt, i)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return frequencyChanges
}

func printResultFrequency(frequencyChanges []int64) {
	var frequency int64
	frequency = 0

	for _, i := range frequencyChanges {
		frequency += i
		log.Printf("frequency: %v", frequency)
	}

	log.Printf("result frequency: %v", frequency)
}

func printFrequencyRepeat(frequencyChanges []int64) {
	var frequency int64
	frequency = 0
	frequencyMap := map[int64]bool{0: true}
	for j := 0; j < 200; j++ {
		for _, i := range frequencyChanges {
			frequency += i
			log.Printf("frequency: %v", frequency)
			if frequencyMap[frequency] {
				log.Printf("at cycle %v frequency %v appeared twice", j, frequency)
				return
			}
			frequencyMap[frequency] = true
		}
	}
}

func main() {
	frequencyChanges := getFrequencyChanges()

	//printResultFrequency(frequencyChanges)
	printFrequencyRepeat(frequencyChanges)
}
