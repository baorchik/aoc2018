package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestKnownData1(t *testing.T) {
	s := sample{
		before:      []int{3, 2, 1, 1},
		instruction: []int{9, 2, 1, 2},
		after:       []int{3, 2, 2, 1}}

	samples := []sample{s}
	codesMap := getOpcodes(samples)

	fmt.Printf("codes: %+v\n", codesMap[0])
	assert.Equal(t, 3, len(codesMap[0]))

	assert.Contains(t, codesMap[0], 2) //"mulr"
	assert.Contains(t, codesMap[0], 1) //addi
	assert.Contains(t, codesMap[0], 9) //"seti"
}
