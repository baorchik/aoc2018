package main

import (
	"bufio"
	"fmt"
	"os"
)

const (
	empty int = iota + 1
	wall
	elf
	goblin
)

type cell struct {
	cType     int
	u         *unit
	isVisited bool
}

type point struct {
	x int
	y int
}

type unit struct {
	hp     int
	uType  int
	p      point
	isDead bool
}

func (u *unit) isActive() bool {
	if u == nil {
		return false
	}
	return !u.isDead
}

type field [][]*cell
type units []*unit

var elfAttack = 3

func stringsToField(strs []string) field {
	f := field{}
	for y := 0; y < len(strs); y++ {
		r := []*cell{}
		str := strs[y]
		for i := 0; i < len(str); i++ {
			c := cell{}
			switch string(str[i]) {
			case "#":
				c.cType = wall
			case ".":
				c.cType = empty
			case "G":
				u := unit{
					uType: goblin,
					hp:    200,
					p:     point{x: i, y: y}}
				c.cType = u.uType
				c.u = &u
			case "E":
				u := unit{
					uType: elf,
					hp:    200,
					p:     point{x: i, y: y}}
				c.cType = u.uType
				c.u = &u
			}
			r = append(r, &c)
		}
		f = append(f, r)
	}
	return f
}

func getField(filename string) field {
	f, err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)

	strs := []string{}
	for scanner.Scan() {
		//fmt.Println(scanner.Text())
		strs = append(strs, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}

	return stringsToField(strs)
}

func printField(f field) {
	fmt.Println()
	for y := 0; y < len(f); y++ {
		for x := 0; x < len(f[y]); x++ {
			switch f[y][x].cType {
			case wall:
				fmt.Printf("#")
			case empty:
				fmt.Printf(".")
			case elf:
				fmt.Printf("E")
			case goblin:
				fmt.Printf("G")
			}
		}
		fmt.Println()
	}
	fmt.Println()
}

type node struct {
	p    point
	prev *node
}

func abs(a int) int {
	if a < 0 {
		return -a
	}
	return a
}

func getBackwordPath(n node) []point {
	path := []point{}
	for {
		if n.prev == nil {
			break
		}
		n = *n.prev
		path = append(path, n.p)
	}
	return path
}

func getNextNodes(n *node) []node {
	nextNodes := []node{}
	nextNodes = append(nextNodes, node{p: point{x: n.p.x, y: n.p.y - 1}, prev: n})
	nextNodes = append(nextNodes, node{p: point{x: n.p.x - 1, y: n.p.y}, prev: n})
	nextNodes = append(nextNodes, node{p: point{x: n.p.x + 1, y: n.p.y}, prev: n})
	nextNodes = append(nextNodes, node{p: point{x: n.p.x, y: n.p.y + 1}, prev: n})
	return nextNodes
}

func setCellsToNotVisited(f field) {
	for y := 0; y < len(f); y++ {
		for x := 0; x < len(f[y]); x++ {
			f[y][x].isVisited = false
		}
	}
}

func findPaths(f field, p point, target point) [][]point {
	var initNodes []node

	setCellsToNotVisited(f)

	nextInitNodes := []node{node{p: p}}
	f[p.y][p.x].isVisited = true

	paths := [][]point{}
	for {
		if len(paths) > 0 {
			return paths
		}

		if len(nextInitNodes) == 0 {
			return paths
		}

		initNodes = nextInitNodes
		nextInitNodes = []node{}
		for i := 0; i < len(initNodes); i++ {
			initNode := initNodes[i]
			nextNodes := getNextNodes(&initNode)

			nextNodesFiltered := []node{}
			for j := 0; j < len(nextNodes); j++ {
				nextNode := nextNodes[j]
				if nextNode.p.x == target.x && nextNode.p.y == target.y {
					path := append([]point{target}, getBackwordPath(nextNode)...)
					paths = append(paths, path)
					continue
				}

				nextCell := f[nextNode.p.y][nextNode.p.x]
				if nextCell.cType == wall || nextCell.isVisited || nextCell.u.isActive() {
					continue
				}
				nextCell.isVisited = true

				nextNodesFiltered = append(nextNodesFiltered, nextNode)
			}
			nextInitNodes = append(nextInitNodes, nextNodesFiltered...)
		}
	}
}

func getPathsWithBestTarget(paths [][]point) [][]point {
	if len(paths) == 0 {
		return [][]point{}
	}
	minY := 999999

	for i := 0; i < len(paths); i++ {
		tPoint := paths[i][0]
		if tPoint.y < minY {
			minY = tPoint.y
		}
	}

	minX := 999999
	for i := 0; i < len(paths); i++ {
		tPoint := paths[i][0]
		if tPoint.y == minY {
			if tPoint.x < minX {
				minX = tPoint.x
			}
		}
	}

	filtered := [][]point{}
	for i := 0; i < len(paths); i++ {
		tPoint := paths[i][0]
		if tPoint.y == minY && tPoint.x == minX {
			filtered = append(filtered, paths[i])
		}
	}
	return filtered
}

func getPathsWithBestFirstStep(paths [][]point) [][]point {
	if len(paths) == 0 {
		return [][]point{}
	}
	minY := 999999
	for i := 0; i < len(paths); i++ {
		tPoint := paths[i][len(paths[i])-2]
		if tPoint.y < minY {
			minY = tPoint.y
		}
	}

	minX := 999999
	for i := 0; i < len(paths); i++ {
		tPoint := paths[i][len(paths[i])-2]
		if tPoint.y == minY {
			if tPoint.x < minX {
				minX = tPoint.x
			}
		}
	}

	filtered := [][]point{}
	for i := 0; i < len(paths); i++ {
		tPoint := paths[i][len(paths[i])-2]
		if tPoint.y == minY && tPoint.x == minX {
			filtered = append(filtered, paths[i])
		}
	}
	return filtered
}

func getPathWithBestSteps(paths [][]point) []point {
	if len(paths) == 0 {
		return []point{}
	}

	paths = getPathsWithBestTarget(paths)
	paths = getPathsWithBestFirstStep(paths)

	return paths[0]
}

func getPath(f field, p point, pTarget point) []point {
	//fmt.Printf("getPath from (%v) to (%v)\n", p, pTarget)

	pointsNextToTarget := []point{
		point{x: pTarget.x, y: pTarget.y - 1},
		point{x: pTarget.x - 1, y: pTarget.y},
		point{x: pTarget.x + 1, y: pTarget.y},
		point{x: pTarget.x, y: pTarget.y + 1}}
	filteredPintsForPath := []point{}

	for i := 0; i < len(pointsNextToTarget); i++ {
		if pointsNextToTarget[i].y == p.y && pointsNextToTarget[i].x == p.x {
			return []point{p}
		}

		cell := f[pointsNextToTarget[i].y][pointsNextToTarget[i].x]
		if cell.cType == wall || cell.u.isActive() {
			continue
		}
		filteredPintsForPath = append(filteredPintsForPath, pointsNextToTarget[i])
	}

	paths := [][]point{}
	for i := 0; i < len(filteredPintsForPath); i++ {
		paths = append(paths, findPaths(f, p, filteredPintsForPath[i])...)
	}

	if len(paths) == 0 {
		return []point{}
	}

	minLen := len(paths[0])
	for i := 0; i < len(paths); i++ {
		if len(paths[i]) < minLen {
			minLen = len(paths[i])
		}
	}

	shortestPaths := [][]point{}
	for i := 0; i < len(paths); i++ {
		if len(paths[i]) == minLen {
			shortestPaths = append(shortestPaths, paths[i])
		}
	}

	//fmt.Printf("shortestPaths:(%v)\n", shortestPaths)
	return getPathWithBestSteps(shortestPaths)
}

func doAttack(f field, u unit, enemiesToAttack units) {
	minHp := 201
	var target *unit
	for i := 0; i < len(enemiesToAttack); i++ {
		e := enemiesToAttack[i]
		if e.hp < minHp {
			minHp = e.hp
			target = e
		}
	}

	if u.uType == elf {
		(*target).hp -= elfAttack
	} else if u.uType == goblin {
		(*target).hp -= 3
	}
	//target.hp -= 3
	//fmt.Printf("%+v attacks %+v, new hp: %d\n", u.p, (*target).p, (*target).hp)
	if target.hp <= 0 {
		//fmt.Printf("%+v dies\n", (*target).p)
		f[target.p.y][target.p.x].cType = empty
		f[target.p.y][target.p.x].u = nil
		target.isDead = true
		return
	}
}

func getEnemiesToAttack(f field, u unit) (units, bool) {
	enemiesToAttack := units{}

	enemies := getEnemies(u.uType, f)
	if len(enemies) == 0 {
		fmt.Println("no more enemies!")
		return enemiesToAttack, true
	}

	for i := 0; i < len(enemies); i++ {
		enemy := enemies[i]
		path := getPath(f, u.p, enemy.p)
		if len(path) == 0 {
			continue
		}

		if len(path) == 1 {
			enemiesToAttack = append(enemiesToAttack, enemy)
		}
	}
	return enemiesToAttack, false
}

func getNextPoint(f field, u unit) (point, bool) {
	exists := false
	nextPosition := point{x: -1}
	//var target unit
	minDistance := 999999
	enemies := getEnemies(u.uType, f)

	for i := 0; i < len(enemies); i++ {
		enemy := *enemies[i]
		path := getPath(f, u.p, enemy.p)
		if len(path) == 0 || len(path) == 1 {
			continue
		}

		if len(path) < minDistance {
			minDistance = len(path)
			nextPosition = path[len(path)-2]
			//target = enemy
			exists = true
			continue
		}
	}

	// if exists {
	// 	fmt.Printf("%+v steps to %+v, moves to unit %+v\n", u.p, nextPosition, target.p)
	// } else {
	// 	fmt.Printf("%+v does nothing\n", u.p)
	// }
	return nextPosition, exists
}

func getEnemies(uType int, f field) units {
	enemies := units{}
	unts := getUnitsInOrder(f)
	for i := 0; i < len(unts); i++ {
		if unts[i].uType == uType {
			continue
		}
		enemies = append(enemies, unts[i])
	}
	return enemies
}

func unitDoAction(f field, u unit) bool {
	enemiesToAttack, stop := getEnemiesToAttack(f, u)
	if len(enemiesToAttack) == 0 && stop {
		fmt.Println("no more enemies!")
		return true
	}

	if len(enemiesToAttack) > 0 {
		doAttack(f, u, enemiesToAttack)
		return false
	}

	nextPosition, exists := getNextPoint(f, u)
	if !exists {
		return false
	}

	f[u.p.y][u.p.x].u.p = nextPosition
	f[nextPosition.y][nextPosition.x].cType = f[u.p.y][u.p.x].cType
	f[nextPosition.y][nextPosition.x].u = f[u.p.y][u.p.x].u
	f[u.p.y][u.p.x].cType = empty
	f[u.p.y][u.p.x].u = nil

	u = *f[nextPosition.y][nextPosition.x].u
	enemiesToAttack, stop = getEnemiesToAttack(f, u)
	if len(enemiesToAttack) == 0 && stop {
		fmt.Println("no more enemies!")
		return true
	}
	if len(enemiesToAttack) > 0 {
		doAttack(f, u, enemiesToAttack)
	}
	return false
}

func hpSum(unts units) int {
	sum := 0
	for _, u := range unts {
		sum += u.hp
	}
	return sum
}

func doIteration(f field) bool {
	unts := getUnitsInOrder(f)
	for i := 0; i < len(unts); i++ {
		u := *unts[i]

		if u.isDead {
			continue
		}

		stop := unitDoAction(f, u)
		if stop {
			return stop
		}
	}

	return false
}

func runFight(f field) int {
	fullIteration := 0
	for {
		stop := doIteration(f)
		//printField(f)
		if stop {
			return fullIteration
		}
		fullIteration++
		//fmt.Printf("\niteration: %d\n", fullIteration)
	}
}

func getUnitsInOrder(f field) units {
	ordered := units{}
	for y := 0; y < len(f); y++ {
		for x := 0; x < len(f[y]); x++ {
			if f[y][x].u.isActive() {
				ordered = append(ordered, f[y][x].u)
			}
		}
	}
	return ordered
}

func getElvesCount(f field) int {
	count := 0
	unts := getUnitsInOrder(f)
	for i := 0; i < len(unts); i++ {
		if unts[i].uType == elf {
			count++
		}
	}
	return count
}

func main() {

	// f := getField("input.txt")
	// iterations := runFight(f)
	// unts := getUnitsInOrder(f)
	// fmt.Printf("iteration: %d\n", iterations)
	// fmt.Printf("sum: %d\n", hpSum(unts))
	// fmt.Printf("out: %d\n", hpSum(unts)*iterations)

	// it 107
	//part1 250594

	elfAttack = 15
	f := getField("input.txt")
	elvesCount := getElvesCount(f)

	for i := 0; i < 20; i++ {
		fmt.Printf("Fight: %d\n", i)
		f = getField("input.txt")
		iterations := runFight(f)

		elvesCountAfterFight := getElvesCount(f)
		fmt.Printf("elvesCountAfterFight: %d\n", elvesCountAfterFight)
		if elvesCountAfterFight == elvesCount {
			unts := getUnitsInOrder(f)
			fmt.Printf("iteration: %d\n", iterations)
			fmt.Printf("sum: %d\n", hpSum(unts))
			fmt.Printf("out: %d\n", hpSum(unts)*iterations)
			break
		}
		elfAttack++
	}

	//part 2 : 52133
}
