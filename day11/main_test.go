package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestKnownPowerCalculationValues(t *testing.T) {
	assert.Equal(t, 4, getPower(3, 5, 8))
	assert.Equal(t, -5, getPower(122, 79, 57))
	assert.Equal(t, 0, getPower(217, 196, 39))
	assert.Equal(t, 4, getPower(101, 153, 71))
}

func TestKnownGridsValues1(t *testing.T) {
	grid := generateGrid(18)
	p, x, y := getCoords(grid)
	assert.Equal(t, 29, p)
	assert.Equal(t, 33, x)
	assert.Equal(t, 45, y)
}

func TestKnownGridsValues2(t *testing.T) {
	grid := generateGrid(42)
	p, x, y := getCoords(grid)
	assert.Equal(t, 30, p)
	assert.Equal(t, 21, x)
	assert.Equal(t, 61, y)
}

func TestKnownGridsValuesResult(t *testing.T) {
	grid := generateGrid(3031)
	p, x, y := getCoords(grid)
	assert.Equal(t, 30, p)
	assert.Equal(t, 21, x)
	assert.Equal(t, 76, y)
}

func TestPart2KnownGridsValues1(t *testing.T) {
	grid := generateGrid(18)
	p := getCoordsWithSize(grid)
	assert.Equal(t, 113, p.p)
	assert.Equal(t, 90, p.x)
	assert.Equal(t, 269, p.y)
	assert.Equal(t, 16, p.s)
}

func TestPart2KnownGridsValues2(t *testing.T) {
	grid := generateGrid(42)
	p := getCoordsWithSize(grid)
	assert.Equal(t, 119, p.p)
	assert.Equal(t, 232, p.x)
	assert.Equal(t, 251, p.y)
	assert.Equal(t, 12, p.s)
}
