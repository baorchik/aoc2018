package main

import (
	"fmt"
	"strings"
)

func printStatus(receipts []int, e1, e2 int) {
	var res string
	for i := 0; i < len(receipts); i++ {
		if i == e1 {
			res += fmt.Sprintf("(%d) ", receipts[i])
		} else if i == e2 {
			res += fmt.Sprintf("[%d] ", receipts[i])
		} else {
			res += fmt.Sprintf(" %d  ", receipts[i])
		}
	}
	fmt.Println(res)
}

func printScores(receipts []int, start int) string {
	var res string
	for i := 0; i < 10; i++ {
		res += fmt.Sprint(receipts[start+i])
	}
	fmt.Printf("scores: %s\n", res)
	return res
}

func makeRecepies(receipts []int, e1, e2, n int) string {
	for {
		//getMoreReceipts
		sum := receipts[e1] + receipts[e2]
		r1 := sum / 10
		r2 := sum % 10
		if r1 != 0 {
			receipts = append(receipts, r1)
		}
		receipts = append(receipts, r2)

		//move elves
		e1 = (receipts[e1] + 1 + e1) % len(receipts)
		e2 = (receipts[e2] + 1 + e2) % len(receipts)
		if e1 == e2 {
			panic("e1==e2!")
		}
		//printStatus(receipts, e1, e2)

		if len(receipts) >= n+10 {
			return printScores(receipts, n)
		}
	}
}

func receiptsToString(receipts []int) string {
	var res string
	for i := 0; i < len(receipts); i++ {
		res += fmt.Sprint(receipts[i])
	}
	return res
}

func makeRecepies2(receipts []int, e1, e2 int, score string) int {
	var compareSample string
	latestRecs := receipts
	for {
		//getMoreReceipts
		sum := receipts[e1] + receipts[e2]
		r1 := sum / 10
		r2 := sum % 10
		if r1 != 0 {
			receipts = append(receipts, r1)
			latestRecs = append(latestRecs, r1)
		}
		receipts = append(receipts, r2)
		latestRecs = append(latestRecs, r2)

		//move elves
		e1 = (receipts[e1] + 1 + e1) % len(receipts)
		e2 = (receipts[e2] + 1 + e2) % len(receipts)
		if e1 == e2 {
			panic("e1==e2!")
		}

		if len(receipts)%100000 <= 1 {
			fmt.Println(len(receipts))
		}

		if len(latestRecs) > 9 {
			latestRecs = latestRecs[len(latestRecs)-9:]
		}

		compareSample = receiptsToString(latestRecs)
		if strings.Contains(compareSample, score) {
			//20200000
			fmt.Println("Substring is found!")
			//recString := receiptsToString(receipts)
			if len(receipts) < 9 {
				idx := strings.Index(compareSample, score)
				fmt.Printf("score: %d\n", idx)
				return idx
			}

			idx := len(receipts) - (9 - strings.Index(compareSample, score))
			fmt.Printf("score: %d\n", idx)
			return idx
		}
	}
	return 0
}

func main() {
	receipts := []int{3, 7}
	e1 := 0
	e2 := 1
	printStatus(receipts, e1, e2)
	//makeRecepies(receipts, e1, e2, 409551)
	// part1 - 1631191756
	makeRecepies2(receipts, e1, e2, "409551")

	//part2 - 20219475

}
