package main

import (
	"fmt"
	"log"
)

const GRID_SIZE = 300

func getPower(x, y, serial int) int {
	rackID := x + 10
	powerLevel := rackID*y + serial
	powerLevel = powerLevel * rackID
	powerLevel = (powerLevel/100)%10 - 5
	return powerLevel
}

func generateGrid(serial int) [][]int {
	grid := [][]int{}
	for y := 0; y <= GRID_SIZE; y++ {
		grid = append(grid, []int{})
		for x := 0; x <= GRID_SIZE; x++ {
			grid[y] = append(grid[y], 0)
		}
	}

	for y := 1; y <= GRID_SIZE; y++ {
		for x := 1; x <= GRID_SIZE; x++ {
			grid[x][y] = getPower(x, y, serial)
		}
	}
	//printGrid(grid)
	return grid
}

func getPowerSize(grid [][]int, x0, y0, size int) int {
	totalPower := 0
	for i := x0; i < x0+size; i++ {
		for j := y0; j < y0+size; j++ {
			totalPower += grid[i][j]
		}
	}
	return totalPower
}

func getMaxP(grid [][]int, i, j, s int, maxP powerCell) powerCell {
	p := getPowerSize(grid, i, j, s)
	if p > maxP.p {
		maxP.p = p
		maxP.x = i
		maxP.y = j
		maxP.s = s
		//log.Printf("Set max power to %+v", maxP)
	}
	return maxP
}

func printGrid(grid [][]int) {
	for y := 0; y < GRID_SIZE; y++ {
		for x := 0; x <= GRID_SIZE; x++ {
			fmt.Printf(" %02d ", grid[x][y])
		}
		fmt.Println()
	}
	fmt.Println("======================")
}

func getCoords(grid [][]int) (int, int, int) {
	maxX, maxY, maxP := 0, 0, 0
	for i := 0; i <= GRID_SIZE-3; i++ {
		for j := 0; j <= GRID_SIZE-3; j++ {
			p := getPowerSize(grid, i, j, 3)
			if p > maxP {
				maxP = p
				maxX = i
				maxY = j
				//log.Printf("Set max power to %d, point (%d, %d)", maxP, maxX, maxY)
			}
		}
	}
	return maxP, maxX, maxY
}

type powerCell struct {
	p int
	x int
	y int
	s int
}

func getCoordsWithSize(grid [][]int) powerCell {
	maxP := powerCell{}
	for s := 1; s <= GRID_SIZE; s++ {
		log.Printf("--- size %d", s)
		for i := 0; i <= GRID_SIZE-s; i++ {
			for j := 0; j <= GRID_SIZE-s; j++ {
				inP := getMaxP(grid, i, j, s, maxP)
				if inP.p > maxP.p {
					maxP.p = inP.p
					maxP.x = inP.x
					maxP.y = inP.y
					maxP.s = inP.s
					//log.Printf("[RES] Set max power to %+v", maxP)
				}
			}
		}
	}
	return maxP
}

func main() {
	// serial := 3031 // part1
	grid := generateGrid(3031)
	p, x, y := getCoords(grid)
	log.Printf("===> Max power %d, point (%d, %d)", p, x, y)

	pS := getCoordsWithSize(grid)
	log.Printf("===> Max p %+v", pS)

}
