package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

type stringrule struct {
	state string
	res   string
}

type rule struct {
	state []bool
	res   bool
}

func getRules(filename string) []stringrule {
	rules := []stringrule{}
	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		r := stringrule{}
		log.Println(scanner.Text())
		_, err := fmt.Sscanf(scanner.Text(),
			"%s => %s",
			&r.state, &r.res)
		if err != nil {
			log.Fatal(err)
		}
		rules = append(rules, r)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return rules
}

func translateString(s string) []bool {
	res := []bool{}
	for _, i := range s {
		if string(i) == "#" {
			res = append(res, true)
		} else {
			res = append(res, false)
		}
	}
	return res
}

func translateRules(rules []stringrule) []rule {
	rulesNew := []rule{}

	for _, r := range rules {

		rNew := rule{
			state: translateString(r.state),
			res:   translateString(r.res)[0]}
		rulesNew = append(rulesNew, rNew)
	}
	return rulesNew
}

func printState(state []bool) string {
	var str string
	for i := 0; i < len(state)-1; i++ {
		if state[i] {
			str += "#"
		} else {
			str += "."
		}
	}
	return str
}

func applyRules(state []bool, rules []rule) []bool {
	newState := []bool{}
	for i := 0; i < len(state)-1; i++ {
		newState = append(newState, false)
	}
	for i := 2; i < len(state)-3; i++ {
		for _, r := range rules {
			if state[i-2] == r.state[0] &&
				state[i-1] == r.state[1] &&
				state[i] == r.state[2] &&
				state[i+1] == r.state[3] &&
				state[i+2] == r.state[4] {
				newState[i] = r.res
			}
		}
	}

	return newState
}

var shiftLeft int

func extendStateWithPots(state []bool) []bool {
	freePotsLeft := true
	freePotsRight := true
	for i := 0; i < 5; i++ {
		if state[i] {
			freePotsLeft = false
		}
		if state[len(state)-i-1] {
			freePotsRight = false
		}
	}
	if !freePotsLeft {
		state = append([]bool{false, false, false, false, false}, state...)
		shiftLeft += 5
	}
	if !freePotsRight {
		state = append(state, []bool{false, false, false, false, false}...)
	}

	return state
}

func getSum(state []bool) int {
	log.Printf("shift: %d", shiftLeft)
	sum := 0
	first := true
	for i := 0; i < len(state)-1; i++ {
		if state[i] {
			if first {
				log.Printf("first index: %d", i)
				first = false
			}
			sum += i - shiftLeft
		}
	}
	return sum
}

func main() {
	//stringrules := getRules("input_example.txt")
	//state := translateString("#..#.#..##......###...###")

	stringrules := getRules("input.txt")
	state := translateString("##....#.#.#...#.#..#.#####.#.#.##.#.#.#######...#.##....#..##....#.#..##.####.#..........#..#...#")

	state = extendStateWithPots(state)

	log.Printf("rules: %+v\n", stringrules)
	rules := translateRules(stringrules)
	log.Printf("rules: %+v\n", rules)

	newState := state
	log.Printf("i: --, oldState: %s\n", printState(state))
	for i := 1; i <= 1002; i++ {
		newState = applyRules(newState, rules)
		newState = extendStateWithPots(newState)
		//log.Printf("i: %0002d, newState: %s\n", i, printState(newState))
	}
	log.Printf("newState: %s\n", printState(newState))
	sum := getSum(newState)
	//shiftLeft = 3
	//sum := getSum(".#....##....#####...#######....#.#..##.")
	log.Printf("sum: %d\n", sum)
	//part1 2349
}

// part 2
// 1000 step =>
// (50000000000 - 1000)*42 = 2099999958000 + 43168 = 2100000001168
