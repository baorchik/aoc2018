package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"sort"
	"strconv"
	"time"
)

type eventType int

const (
	shift  eventType = 0
	asleep eventType = 1
	awake  eventType = 2
)

type event struct {
	time      time.Time
	eventType eventType
	guard     int64
}

type guardHourStat []int

type guardTime map[int64]guardHourStat

func lineToEvent(str string) event {
	r, err := regexp.Compile("\\[([^\\]]*)\\] (.*)")
	if err != nil {
		log.Fatal(err)
	}
	res := r.FindStringSubmatch(str)
	const customForm = "2006-01-02 15:04"
	date, err := time.Parse(customForm, res[1])
	if err != nil {
		log.Println(res[1])
		log.Fatal(err)
	}
	text := res[2]

	e := event{time: date}
	switch text {
	case "falls asleep":
		e.eventType = asleep
	case "wakes up":
		e.eventType = awake
	default:
		e.eventType = shift
		var g string
		_, err := fmt.Sscanf(text, "Guard #%v begins shift", &g)
		if err != nil {
			log.Fatal(err)
		}
		e.guard, err = strconv.ParseInt(g, 10, 32)
		if err != nil {
			log.Fatal(err)
		}
	}

	printEvent(e)
	return e
}

func getEvents(filename string) []event {
	events := []event{}

	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {

		events = append(events, lineToEvent(scanner.Text()))
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return events
}

func getMinutes(e event) int {
	if e.time.Hour() == 23 {
		return 0
	}
	return e.time.Minute()
}

func getGuardsTime(events []event) guardTime {
	stats := guardTime{}
	var g int64
	for i := 0; i < len(events); i++ {
		e := events[i]
		switch e.eventType {
		case (shift):
			g = e.guard
			if len(stats[g]) == 0 {
				stats[g] = make(guardHourStat, 60)
			}
		case (asleep):
			sleepStart := getMinutes(e)
			if i == len(events)-1 || events[i+1].eventType != awake {
				log.Fatal("wrong sleep sequence!")
			}
			sleepEnds := getMinutes(events[i+1])
			for j := sleepStart; j < sleepEnds; j++ {
				stats[g][j]++
			}
		}
	}
	return stats
}

func printStrategy2(guards guardTime) {
	maxValAbs := 0
	maxMinAbs := 0
	var gAbs int64
	for g, t := range guards {
		maxVal := 0
		maxMin := 0
		for i := 0; i < len(t); i++ {
			if t[i] > maxVal {
				maxVal = t[i]
				maxMin = i
				if maxVal > maxValAbs {
					maxValAbs = maxVal
					maxMinAbs = maxMin
					gAbs = g
				}
			}
		}
		fmt.Printf("g: %d, val: %d, min: %d \n", g, maxVal, maxMin)
	}
	fmt.Printf("ABS: g: %d, val: %d, min: %d\n", gAbs, maxValAbs, maxMinAbs)
	fmt.Printf("Result: %d", gAbs*int64(maxMinAbs))
}

func printStrategy1(guards guardTime) {
	maxDuration := 0
	var gMaxDuration int64
	for g, t := range guards {
		duration := 0
		for i := 0; i < len(t); i++ {
			if t[i] > 0 {
				duration += t[i]
			}
		}
		if duration > maxDuration {
			maxDuration = duration
			gMaxDuration = g
		}
		fmt.Printf("g: %d, val: %d\n", g, duration)
	}
	fmt.Printf("Duration: g: %d, val: %d\n", gMaxDuration, maxDuration)

	bestTimes := guards[gMaxDuration]
	maxVal := 0
	maxMin := 0
	for i := 0; i < len(bestTimes); i++ {
		if bestTimes[i] > maxVal {
			maxVal = bestTimes[i]
			maxMin = i
		}
	}
	fmt.Printf("Duration: g: %d, min: %d, val: %d\n", gMaxDuration, maxMin, maxVal)
	fmt.Printf("Result: %d", gMaxDuration*int64(maxMin))
}

func printEvent(e event) {
	log.Printf("event: %02d-%02d, %v, %v", e.time.Month(), e.time.Day(), e.guard, e.eventType)
}

func printGuards(g guardTime) {
	mins := []int{}
	for i := 0; i < 60; i++ {
		mins = append(mins, i)
	}

	log.Printf("g: --, %02v", mins)
	for g, t := range g {
		log.Printf("g: %d, %02v", g, t)
	}
}

func main() {
	//events := getEvents("input_example.txt")
	events := getEvents("input.txt")
	sort.Slice(events, func(i, j int) bool { return events[i].time.Before(events[j].time) })
	log.Println("_after sorting_")
	for i := 0; i < len(events); i++ {
		printEvent(events[i])
	}
	guards := getGuardsTime(events)
	printGuards(guards)
	printStrategy1(guards)
	printStrategy2(guards)
}
