package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

type tpoint struct {
	x int
	y int
	z int
}

type tnanobot struct {
	pos tpoint
	r   int
}

type tnanobots []tnanobot

func nanobotFromString(str string) tnanobot {
	var x, y, z, r int
	_, err := fmt.Sscanf(str, "pos=<%d,%d,%d>, r=%d", &x, &y, &z, &r)
	if err != nil {
		panic(err)
	}
	return tnanobot{
		pos: tpoint{x: x, y: y, z: z},
		r:   r}
}

func getData(filename string) tnanobots {
	nanobots := tnanobots{}
	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		if len(scanner.Text()) == 0 {
			continue
		}
		nanobots = append(nanobots, nanobotFromString(scanner.Text()))
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return nanobots
}

func abs(i int) int {
	if i < 0 {
		return -i
	}
	return i
}

func sig(i int) int {
	if i < 0 {
		return -1
	}
	return 1
}

func distance(p1, p2 *tpoint) int {
	return abs(p1.x-p2.x) + abs(p1.y-p2.y) + abs(p1.z-p2.z)
}

func getBotsInRange(bots tnanobots, center tnanobot) tnanobots {
	botsNearby := tnanobots{}
	for _, b := range bots {
		if distance(&b.pos, &center.pos) <= center.r {
			botsNearby = append(botsNearby, b)
		}
	}
	return botsNearby
}

func getMostPowerfulBot(bots tnanobots) {
	maxRange := 0
	var maxRangeBot tnanobot
	for _, b := range bots {
		if b.r > maxRange {
			maxRange = b.r
			maxRangeBot = b
		}
	}
	fmt.Printf("max range bot: %+v\n", maxRangeBot)
	botsInRange := getBotsInRange(bots, maxRangeBot)
	fmt.Printf("botsInRange: %d\n", len(botsInRange))
}

type tfield map[tpoint]int

func pointInRadius(p, p0 *tpoint, r int) bool {
	return distance(p0, p) <= r
}

func pointIsBorder(p, p0 *tpoint, r int) bool {
	return distance(p0, p) == r
}

func scaleBots(bots tnanobots, newZoom int) tnanobots {
	zoomBots := tnanobots{}
	for _, b := range bots {
		zoomBots = append(zoomBots, tnanobot{
			pos: tpoint{b.pos.x / newZoom, b.pos.y / newZoom, b.pos.z / newZoom},
			r:   b.r / newZoom})
	}

	return zoomBots
}

func scaleArea(area []tpoint, oldZoom, newZoom int) []tpoint {
	newArea := []tpoint{}
	for _, p := range area {
		px0 := (p.x * oldZoom) / newZoom
		px1 := ((p.x + sig(p.x)) * oldZoom) / newZoom

		py0 := (p.y * oldZoom) / newZoom
		py1 := ((p.y + sig(p.y)) * oldZoom) / newZoom

		pz0 := (p.z * oldZoom) / newZoom
		pz1 := ((p.z + sig(p.z)) * oldZoom) / newZoom

		for x := px0; x <= px1; x++ {
			for y := py0; y <= py1; y++ {
				for z := pz0; z <= pz1; z++ {
					newArea = append(newArea, tpoint{x, y, z})
				}
			}
		}
	}
	fmt.Printf("newArea len after scaling: %d\n", len(newArea))
	return newArea
}

func getArea(bots tnanobots, area []tpoint, oldZoom, newZoom int) []tpoint {
	fmt.Printf("getArea. oldZoom: %d, newZoom: %d\n", oldZoom, newZoom)
	//fmt.Printf("getArea. minDistancePoint: %v\n", area)
	field := tfield{}

	bots = scaleBots(bots, newZoom)
	if len(area) == 0 {
		for _, b := range bots {
			for x := (b.pos.x - b.r) / newZoom; x <= (b.pos.x+b.r)/newZoom; x++ {
				for y := (b.pos.y - b.r) / newZoom; y <= (b.pos.y+b.r)/newZoom; y++ {
					for z := (b.pos.z - b.r) / newZoom; z <= (b.pos.z+b.r)/newZoom; z++ {
						pp := tpoint{x, y, z}
						if pointIsBorder(&pp, &b.pos, b.r) {
							field[pp]++
						}
					}
				}
			}
		}
	} else {
		area = scaleArea(area, oldZoom, newZoom)
		for aIdx := range area {
			for bIdx := range bots {
				if pointIsBorder(&area[aIdx], &bots[bIdx].pos, bots[bIdx].r) {
					field[area[aIdx]]++
				}
			}
		}
	}

	maxVal := 0
	for _, v := range field {
		if v > maxVal {
			maxVal = v
		}
	}

	fmt.Printf("maxVal: %d\n", maxVal)

	newArea := []tpoint{}
	for p, v := range field {
		if v == maxVal {
			newArea = append(newArea, p)
		}
	}
	//fmt.Printf("newArea: %v\n", newArea)
	fmt.Printf("newArea len: %d\n", len(newArea))

	if len(newArea) == 0 {
		panic("new area is empty!")
	}

	return newArea
}

func printMinDistancePoint(area []tpoint) {
	center := tpoint{x: 0, y: 0, z: 0}

	var minDistancePoint tpoint
	minDistance := 99999999999
	for _, p := range area {
		d := distance(&p, &center)
		if d < minDistance {
			minDistance = d
			minDistancePoint = p
		}
	}

	fmt.Printf("minDistancePoint : %v\n", minDistancePoint)
	fmt.Printf("min distance : %d\n", minDistance)

}

func printBestPoint(bots tnanobots) {
	area := []tpoint{}

	oldZoom := 0
	for newZoom := 100000000; newZoom > 0; newZoom = newZoom / 10 {
		area = getArea(bots, area, oldZoom, newZoom)
		oldZoom = newZoom
	}

	printMinDistancePoint(area)
}

func main() {
	//bots := getData("input_example3.txt")
	//bots := getData("input_example2.txt")
	bots := getData("input.txt")
	fmt.Printf("bots: %+v\n", bots)
	getMostPowerfulBot(bots)

	//part1 - 730

	printBestPoint(bots)

	//part2
	// 46125210 too low
	// 46005301 too low
	// 48202277 too low
	// 48202318 is wrong
}
