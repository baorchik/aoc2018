package main

import (
	"fmt"
	"strings"
)

type tpoint struct {
	x int
	y int
}

type tregion struct {
	rtype         int
	erosionLevel  int
	geologicIndex int
}

// region type
const (
	rocky int = iota + 1
	narrow
	wet
)

type tfield map[tpoint]tregion
type tvisited map[tpoint]map[int]int
type tvisitedstate struct {
	wastedMins int
	tool       int
}

// tools type
const (
	neither int = iota + 1
	torch
	climbingGear
)

type tstate struct {
	wastedMins int
	tool       int
	p          *tpoint
	prev       *tstate
}

func (s *tstate) getNewState(f tfield, toP tpoint) *tstate {
	wastedMins := s.wastedMins + 1
	currentArea, ok := f[*s.p]
	if !ok {
		panic(fmt.Sprintf("error retriving field for %v", s.p))
	}

	nextArea, ok := f[toP]
	if !ok {
		panic(fmt.Sprintf("error retriving field for %v", toP))
	}
	newtool := switchToolForArea(currentArea.rtype, nextArea.rtype, s.tool)
	if newtool != s.tool {
		wastedMins += 7
	}

	return &tstate{
		wastedMins: wastedMins,
		tool:       newtool,
		p:          &toP,
		prev:       s}
}

func switchToolForArea(fromArea, toArea, fromTool int) int {
	switch fromArea {
	case rocky: // possible tools : climbingGear, torch
		switch toArea {
		case rocky:
			return fromTool
		case wet:
			return climbingGear
		case narrow:
			return torch
		}
	case wet: // possible tools : climbingGear, neither
		switch toArea {
		case rocky:
			return climbingGear
		case wet:
			return fromTool
		case narrow:
			return neither
		}
	case narrow: // possible tools : torch, neither
		switch toArea {
		case rocky:
			return torch
		case wet:
			return neither
		case narrow:
			return fromTool
		}
	default:
		panic(fmt.Sprintf("wrong area type %d", fromArea))
	}
	return 0
}

func getGeologicIndex(p, target *tpoint, f tfield) int {
	if p.x == 0 && p.y == 0 {
		return 0
	}
	if p.x == target.x && p.y == target.y {
		return 0
	}

	if p.y == 0 {
		return p.x * 16807
	}

	if p.x == 0 {
		return p.y * 48271
	}

	return f[tpoint{x: p.x - 1, y: p.y}].erosionLevel * f[tpoint{x: p.x, y: p.y - 1}].erosionLevel
}

func fillRegions(depth int, target *tpoint) tfield {
	f := tfield{}
	setMaxP(target)
	for y := 0; y <= maxP.y; y++ {
		for x := 0; x <= maxP.x; x++ {
			p := tpoint{x: x, y: y}
			region := tregion{
				geologicIndex: getGeologicIndex(&p, target, f)}

			region.erosionLevel = (region.geologicIndex + depth) % 20183

			switch region.erosionLevel % 3 {
			case 0:
				region.rtype = rocky
			case 1:
				region.rtype = wet
			case 2:
				region.rtype = narrow
			}
			f[p] = region
		}
	}
	return f
}

func printField(f tfield, target *tpoint) string {
	var sb strings.Builder
	setMaxP(target)
	for y := 0; y <= maxP.y; y++ {
		for x := 0; x <= maxP.x; x++ {

			if x == 0 && y == 0 {
				sb.WriteString(" M ")
				continue
			}

			if x == target.x && y == target.y {
				sb.WriteString(" T ")
				continue
			}

			var c string
			switch f[tpoint{x: x, y: y}].rtype {
			case rocky:
				c = " . "
			case wet:
				c = " = "
			case narrow:
				c = " | "
			}
			sb.WriteString(c)
		}
		sb.WriteString("\n")
	}

	return sb.String()
}

func getRisk(f tfield, target tpoint) int {
	risk := 0
	for y := 0; y <= target.y; y++ {
		for x := 0; x <= target.x; x++ {
			p := tpoint{x: x, y: y}
			switch f[p].rtype {
			case wet:
				risk++
			case narrow:
				risk += 2
			}
		}
	}
	return risk
}

var maxP tpoint

func setMaxP(target *tpoint) {
	extension := 10
	maxP = tpoint{x: target.x * extension, y: target.y * extension}
}

func setMaxMins(target *tpoint) int {
	return (target.x + target.y) * 8
	//return maxLenghtToTargetInMins(&tpoint{x: 0, y: 0}, target)
}

func printPath(s *tstate) {
	for s != nil {
		fmt.Printf("[(%d,%d) tool %d, mins %d]\n", s.p.x, s.p.y, s.tool, s.wastedMins)
		s = s.prev
	}
	fmt.Println()
}

type tstates []*tstate

func abs(a int) int {
	if a < 0 {
		return -a
	}
	return a
}

func maxLenghtToTargetInMins(p, target *tpoint) int {
	return minLenghtToTargetInMins(p, target) * 8
}

func minLenghtToTargetInMins(p, target *tpoint) int {
	return abs(target.x-p.x) + abs(target.y-p.y)
}
func addNextStates(f tfield, target *tpoint, s *tstate, maxMins *int, visited tvisited) tstates {
	areasToVisit := []tpoint{}

	if s.p.x > 0 {
		areasToVisit = append(areasToVisit, tpoint{x: s.p.x - 1, y: s.p.y})
	}

	if s.p.x < maxP.x {
		areasToVisit = append(areasToVisit, tpoint{x: s.p.x + 1, y: s.p.y})
	}

	if s.p.y > 0 {
		areasToVisit = append(areasToVisit, tpoint{x: s.p.x, y: s.p.y - 1})
	}

	if s.p.y < maxP.y {
		areasToVisit = append(areasToVisit, tpoint{x: s.p.x, y: s.p.y + 1})
	}

	nextStates := tstates{}
	for _, newP := range areasToVisit {
		newState := s.getNewState(f, newP)
		if newState.wastedMins > *maxMins {
			continue
		}

		visitedState, ok := visited[newP]
		if ok {
			wastedMins, ok := visitedState[newState.tool]
			if ok && newState.wastedMins >= wastedMins {
				continue
			}
		} else {
			visited[newP] = map[int]int{}
		}

		visited[newP][newState.tool] = newState.wastedMins

		nextStates = append(nextStates, newState)

		if *newState.p == *target {
			if newState.tool != torch {
				newState.tool = torch
				newState.wastedMins += 7
			}
			if newState.wastedMins < *maxMins {
				*maxMins = newState.wastedMins
				// printPath(newState)
			}
		}
	}

	// fmt.Printf("=== nexStates: ")
	// for _, s := range nextStates {
	// 	fmt.Printf("%v %d ", *s.p, s.wastedMins)
	// }
	// fmt.Println()
	// fmt.Printf("=== visited: %v\n", visited)

	return nextStates
}

func findPath(f tfield, target *tpoint) int {
	setMaxP(target)
	maxMins := setMaxMins(target)

	start := tpoint{x: 0, y: 0}
	s := tstate{
		prev:       nil,
		tool:       torch,
		p:          &start,
		wastedMins: 0}

	visited := tvisited{}
	visited[*s.p] = map[int]int{}
	visited[*s.p][s.tool] = s.wastedMins
	currentStates := tstates{&s}
	nexStates := tstates{}
	for {
		nexStates = tstates{}
		for _, s := range currentStates {
			nexStates = append(nexStates, addNextStates(f, target, s, &maxMins, visited)...)
		}

		// fmt.Println()
		fmt.Printf("*** maxMins: %d, visited len: %d, nexStates len: %d\n", maxMins, len(visited), len(nexStates))

		// fmt.Printf("*** nexStates: ")
		// for _, s := range nexStates {
		// 	fmt.Printf("%v (%d, %d) ", *s.p, s.wastedMins, s.tool)
		// }
		// fmt.Println()
		// fmt.Printf("visited: %v\n", visited)
		// fmt.Println()

		if len(nexStates) == 0 {
			fmt.Println("No new states!")
			return maxMins
		}
		currentStates = nexStates
	}
}

func main() {
	depth := 11739
	target := tpoint{x: 11, y: 718}
	f := fillRegions(depth, &target)
	fprint := printField(f, &target)
	fmt.Println(fprint)
	risk := getRisk(f, target)
	fmt.Printf("risk: %d", risk)

	//part1 - 8735
	mins := findPath(f, &target)

	fmt.Printf("mins: %d\n", mins)
	//part2 - 984
}
