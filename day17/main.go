package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const (
	empty int = iota + 1
	cClay
	spring
	movingWater
	stillWater
)

type cell struct {
	t int
}

type tPoint struct {
	x int
	y int
}

type tDrop struct {
	dType     int
	p         tPoint
	nextDrops []*tDrop
	prevDrop  *tDrop
}

type tSpring struct {
	p    tPoint
	drop *tDrop
}

type clay struct {
	x0 int64
	x1 int64
	y0 int64
	y1 int64
}

func getData(filename string) ([]clay, int64, int64) {
	clays := []clay{}
	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)

	r, err := regexp.Compile("(.*)=(\\d+), (.*)=(\\d+)\\.\\.(\\d+)")
	if err != nil {
		panic(err)
	}

	var maxX, maxY int64
	for scanner.Scan() {
		if len(scanner.Text()) == 0 {
			continue
		}

		res := r.FindStringSubmatch(scanner.Text())
		c := clay{}
		if res[1] == "x" {
			c.x0, err = strconv.ParseInt(res[2], 10, 32)
			if err != nil {
				panic(err)
			}
			if c.x0 > maxX {
				maxX = c.x0
			}

			c.y0, err = strconv.ParseInt(res[4], 10, 32)
			if err != nil {
				panic(err)
			}
			if c.y0 > maxY {
				maxY = c.y0
			}

			c.y1, err = strconv.ParseInt(res[5], 10, 32)
			if err != nil {
				panic(err)
			}
			if c.y1 > maxY {
				maxY = c.y1
			}
		} else if res[1] == "y" {
			c.y0, err = strconv.ParseInt(res[2], 10, 32)
			if err != nil {
				panic(err)
			}
			if c.y0 > maxY {
				maxY = c.y0
			}

			c.x0, err = strconv.ParseInt(res[4], 10, 32)
			if err != nil {
				panic(err)
			}
			if c.x0 > maxX {
				maxX = c.x0
			}

			c.x1, err = strconv.ParseInt(res[5], 10, 32)
			if err != nil {
				panic(err)
			}
			if c.x1 > maxX {
				maxX = c.x1
			}
		} else {
			panic("unexpected input")
		}
		clays = append(clays, c)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return clays, maxX, maxY
}

type field [][]cell

func getField(clays []clay, maxX, maxY int) field {
	f := field{}
	for y := 0; y <= maxY; y++ {
		r := []cell{}
		for x := 0; x <= maxX; x++ {
			r = append(r, cell{t: empty})
		}
		f = append(f, r)
	}

	for _, c := range clays {
		if c.y1 == 0 {
			for x := c.x0; x <= c.x1; x++ {
				f[c.y0][x].t = cClay
			}
		}
		if c.x1 == 0 {
			for y := c.y0; y <= c.y1; y++ {
				f[y][c.x0].t = cClay
			}
		}
	}
	return f
}

func printField(f field) {
	t := time.Now()
	file, err := os.Create(fmt.Sprintf("%02d-%02d-%02d-%d.txt",
		t.Hour(), t.Minute(), t.Second(), t.UnixNano()))
	if err != nil {
		log.Fatal(err)
	}
	minY := getMinY(f)
	minX := getMinX(f)
	for y := minY; y < len(f); y++ {
		var sb strings.Builder
		for x := minX; x < len(f[y]); x++ {
			switch f[y][x].t {
			case cClay:
				sb.WriteString("#")
			case empty:
				sb.WriteString(".")
			case spring:
				sb.WriteString("+")
			case movingWater:
				sb.WriteString("|")
			case stillWater:
				sb.WriteString("~")
			}
		}
		file.WriteString(sb.String())
		file.WriteString(fmt.Sprintln())
	}

	file.WriteString(fmt.Sprintln("---------------"))
	file.WriteString(fmt.Sprintln("---------------"))
	file.WriteString(fmt.Sprintln("---------------"))
}

func growLastDropHorizontally(f field, d *tDrop) int {
	fmt.Printf("grow horizontally x:%d, y:%d\n", d.p.x, d.p.y)
	rightBorderX := -1
	for x := d.p.x; x < len(f[d.p.y]); x++ {
		if f[d.p.y][x].t == cClay {
			rightBorderX = x
			break
		}
	}
	leftBorderX := -1
	for x := d.p.x; x > 0; x-- {
		if f[d.p.y][x].t == cClay {
			leftBorderX = x
			break
		}
	}
	if leftBorderX >= 0 && rightBorderX >= 0 {
		closedSpace := true
		for x := leftBorderX + 1; x <= rightBorderX-1; x++ {
			if f[d.p.y+1][x].t == empty || f[d.p.y+1][x].t == movingWater {
				closedSpace = false
			}
		}
		if closedSpace {
			for x := leftBorderX + 1; x <= rightBorderX-1; x++ {
				if f[d.p.y][x].t == movingWater || f[d.p.y][x].t == empty || f[d.p.y][x].t == stillWater {
					f[d.p.y][x].t = stillWater
					continue
				}
				fmt.Printf("leftBorderX:%d, rightBorderX: %d", leftBorderX, rightBorderX)
				fmt.Printf("x:%d, y: %d", x, d.p.y)
				panic(fmt.Sprintf("overriding cells with type %d (unexpected type)!", f[d.p.y][x].t))
			}

			d.prevDrop.nextDrops = nil
			d = d.prevDrop
			return 1
		}
	}

	growth := 0
	if d.p.x+1 < len(f[d.p.y]) && f[d.p.y][d.p.x+1].t == empty {
		dNew := tDrop{
			dType:     movingWater,
			p:         tPoint{x: d.p.x + 1, y: d.p.y},
			prevDrop:  d,
			nextDrops: nil}
		d.nextDrops = append(d.nextDrops, &dNew)
		f[d.p.y][d.p.x+1].t = movingWater
		growth++
	}
	if d.p.x > 0 && f[d.p.y][d.p.x-1].t == empty {
		dNew := tDrop{
			dType:     movingWater,
			p:         tPoint{x: d.p.x - 1, y: d.p.y},
			prevDrop:  d,
			nextDrops: nil}
		d.nextDrops = append(d.nextDrops, &dNew)
		f[d.p.y][d.p.x-1].t = movingWater
		growth++
	}
	return growth
}

func growLastDrop(f field, d *tDrop) int {
	fmt.Printf("grow x:%d, y:%d\n", d.p.x, d.p.y)
	if d.p.y >= len(f)-1 {
		d.prevDrop.nextDrops = nil
		return 0
	}

	switch f[d.p.y+1][d.p.x].t {
	case empty:
		f[d.p.y+1][d.p.x].t = movingWater
		dNew := tDrop{
			dType:     movingWater,
			p:         tPoint{x: d.p.x, y: d.p.y + 1},
			prevDrop:  d,
			nextDrops: nil}
		d.nextDrops = append(d.nextDrops, &dNew)
		return 1
	case stillWater, cClay:
		return growLastDropHorizontally(f, d)
	case movingWater:
		return 0
	default:
		panic("unknown situation")
	}
}

func getLastDrops(f field, d *tDrop) []*tDrop {
	if d.nextDrops == nil {
		return []*tDrop{d}
	}
	lastDrops := []*tDrop{}
	for _, dn := range d.nextDrops {
		lastDrops = append(lastDrops, getLastDrops(f, dn)...)
	}
	return lastDrops
}

func doIteration(f field, s *tDrop) int {
	drops := getLastDrops(f, s)

	growth := 0
	for _, d := range drops {
		growth += growLastDrop(f, d)
	}
	return growth
}

func getMinY(f field) int {
	for y := 0; y < len(f); y++ {
		for x := 0; x < len(f[y]); x++ {
			if f[y][x].t == cClay {
				return y
			}
		}
	}
	return 0
}

func getMinX(f field) int {
	minX := len(f[0])
	for y := 0; y < len(f); y++ {
		for x := 0; x < len(f[y]); x++ {
			if f[y][x].t == cClay && x < minX {
				minX = x
			}
		}
	}
	return minX
}

func printSum(f field) {
	minY := getMinY(f)

	fmt.Printf("Sum minY: %d\n", minY)

	sum := 0
	for y := minY; y < len(f); y++ {
		for x := 0; x < len(f[y]); x++ {
			switch f[y][x].t {
			case movingWater, stillWater:
				sum++
			}
		}
	}
	fmt.Printf("Sum: %d\n", sum)
}

func printSum2(f field) {
	minY := getMinY(f)

	fmt.Printf("Sum minY: %d\n", minY)

	sum := 0
	for y := minY; y < len(f); y++ {
		for x := 0; x < len(f[y]); x++ {
			switch f[y][x].t {
			case stillWater:
				sum++
			}
		}
	}
	fmt.Printf("Sum: %d\n", sum)
}

func main() {

	//data, maxX, maxY := getData("input_example.txt")
	data, maxX, maxY := getData("input.txt")
	fmt.Printf("%+v\n", data)
	f := getField(data, int(maxX), int(maxY))
	f[0][500].t = spring
	printField(f)

	s := tDrop{
		dType:     spring,
		p:         tPoint{x: 500, y: 0},
		prevDrop:  nil,
		nextDrops: nil}

	for {
		growth := doIteration(f, &s)
		//printField(f)
		if growth == 0 {
			fmt.Println("no growth")
			printField(f)
			break
		}
	}
	printSum(f)
	//part1 - 32439
	printSum2(f)
	//part2 - 26729
}
