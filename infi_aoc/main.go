package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

type tpoint struct {
	x int
	y int
}

type tcell struct {
	p           *tpoint
	runeVal     string
	connections []*tcell
}

type tfield map[tpoint]*tcell

func printField(f tfield, lastp *tpoint) {
	for y := 0; y <= lastp.y; y++ {
		for x := 0; x <= lastp.y; x++ {
			v, ok := f[tpoint{x: x, y: y}]
			if !ok {
				panic("cell doesn't have value!")
			}
			fmt.Print(v.runeVal)
		}
		fmt.Println()
	}
	fmt.Println()

	// for y := 0; y <= lastp.y; y++ {
	// 	for x := 0; x <= lastp.y; x++ {
	// 		v := f[tpoint{x: x, y: y}]
	// 		fmt.Printf("%+v connected to ", v.p)
	// 		for _, c := range v.connections {
	// 			fmt.Printf("%+v ", c.p)
	// 		}
	// 		fmt.Println()
	// 	}
	// }
}

func buildConnections(f tfield, lastp *tpoint) {
	for y := 0; y <= lastp.y; y++ {
		for x := 0; x <= lastp.y; x++ {
			p := tpoint{x: x, y: y}
			f[p].connections = []*tcell{}

			if y > 0 {
				pUp := tpoint{x: x, y: y - 1}
				switch f[p].runeVal {
				//all options: "║", "╔", "╗", "╠", "╦", "╚", "╝", "╬", "╩", "═", "╣":
				case "║", "╠", "╚", "╝", "╬", "╩", "╣":
					switch f[pUp].runeVal {
					case "║", "╔", "╗", "╠", "╦", "╬", "╣":
						f[pUp].connections = append(f[pUp].connections, f[p])
						f[p].connections = append(f[p].connections, f[pUp])
					}
				}
			}
			if x > 0 {
				pLeft := tpoint{x: x - 1, y: y}
				switch f[p].runeVal {
				case "╗", "╦", "╝", "╬", "╩", "═", "╣":
					switch f[pLeft].runeVal {
					case "╔", "╠", "╦", "╚", "╬", "╩", "═":
						f[pLeft].connections = append(f[pLeft].connections, f[p])
						f[p].connections = append(f[p].connections, f[pLeft])
					}
				}
			}
		}
	}
}

func getData(filename string) (tfield, *tpoint) {
	field := tfield{}
	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)

	y := 0
	var lastPoint *tpoint
	for scanner.Scan() {
		if len(scanner.Text()) == 0 {
			continue
		}
		x := 0
		for _, runeVal := range scanner.Text() {
			p := tpoint{x: x, y: y}
			c := tcell{
				p:           &p,
				runeVal:     string(runeVal),
				connections: []*tcell{}}
			field[p] = &c
			lastPoint = &p
			x++
		}
		y++
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	buildConnections(field, lastPoint)
	return field, lastPoint
}

func makeShift(f tfield, lastp *tpoint, step int) {
	if step%2 == 0 {
		x := (step - 1) % (lastp.x + 1)
		fmt.Printf("shift column %d\n", x)
		yL := *f[tpoint{x: x, y: lastp.y}]
		for y := lastp.y; y > 0; y-- {
			p := tpoint{x: x, y: y}
			f[p] = f[tpoint{x: x, y: y - 1}]
			f[p].p = &p
		}
		p := tpoint{x: x, y: 0}
		f[p] = &yL
		f[p].p = &p
	} else {
		y := (step - 1) % (lastp.y + 1)
		fmt.Printf("shift row %d\n", y)
		xL := *f[tpoint{x: lastp.x, y: y}]
		for x := lastp.x; x > 0; x-- {
			p := tpoint{x: x, y: y}
			f[p] = f[tpoint{x: x - 1, y: y}]
			f[p].p = &p
		}
		p := tpoint{x: 0, y: y}
		f[p] = &xL
		f[p].p = &p
	}
	buildConnections(f, lastp)
}

func findPathStepsCount(f tfield, lastPoint *tpoint) int {
	p := tpoint{0, 0}

	visited := map[tpoint]bool{}
	startCells := []*tcell{f[p]}
	var nextCells []*tcell
	steps := 0
	for {
		steps++
		nextCells = []*tcell{}

		for _, c := range startCells {
			visited[*c.p] = true
			nextCells = append(nextCells, c.connections...)
		}
		if len(nextCells) == 0 {
			panic("No more next cells!")
		}

		for _, nc := range nextCells {
			if nc.p.x == lastPoint.x && nc.p.y == lastPoint.y {
				return steps
			}
			if visited[*nc.p] {
				continue
			}
			startCells = append(startCells, nc)
		}
	}
}

func findPathStepsCountWithShift(f tfield, lastPoint *tpoint) int {
	p := tpoint{0, 0}

	startCells := []*tcell{f[p]}
	var nextCells []*tcell
	steps := 0
	for {
		steps++
		nextCells = []*tcell{}
		fmt.Printf("step: %d\n", steps)

		for _, c := range startCells {
			nextCells = append(nextCells, c.connections...)
		}
		if len(nextCells) == 0 {
			panic("No more next cells!")
		}

		fmt.Print("nexCells before shift: ")
		for _, nc := range nextCells {
			fmt.Printf("%+v ", nc.p)
		}
		fmt.Println()

		makeShift(f, lastPoint, steps)
		printField(f, lastPoint)

		startCells = []*tcell{}
		duplicates := map[tpoint]bool{}
		fmt.Print("nexCells after shift filtered: ")
		for _, nc := range nextCells {
			if nc.p.x == lastPoint.x && nc.p.y == lastPoint.y {
				return steps
			}
			if duplicates[*nc.p] {
				continue
			}
			duplicates[*nc.p] = true
			fmt.Printf("%+v ", nc.p)
			startCells = append(startCells, nc)
		}
		fmt.Println()
	}
}

func main() {
	f, lastp := getData("input.txt")
	printField(f, lastp)
	fmt.Printf("lastPoint %+v\n", lastp)
	steps := findPathStepsCountWithShift(f, lastp)
	fmt.Printf("steps %d\n", steps)
}
