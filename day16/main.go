package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
)

type sample struct {
	before      []int
	instruction []int
	after       []int
}

func getInstructionFromString(str string) []int {
	log.Println(str)
	instruction := []int{}
	var v1, v2, v3, v4 int
	_, err := fmt.Sscanf(str, "%d %d %d %d", &v1, &v2, &v3, &v4)
	if err != nil {
		log.Fatal(err)
	}
	instruction = append(instruction, v1, v2, v3, v4)
	return instruction
}

func getStatusFromString(str string) []int {
	log.Println(str)
	status := []int{}
	r, err := regexp.Compile("\\[(.*)\\]")
	if err != nil {
		log.Fatal(err)
	}
	res := r.FindStringSubmatch(str)
	var v1, v2, v3, v4 int
	_, err = fmt.Sscanf(res[1], "%d, %d, %d, %d", &v1, &v2, &v3, &v4)
	if err != nil {
		log.Fatal(err)
	}
	status = append(status, v1, v2, v3, v4)
	return status
}

func getData(filename string) []sample {
	samples := []sample{}
	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		if len(scanner.Text()) == 0 {
			continue
		}
		s := sample{}
		s.before = getStatusFromString(scanner.Text())
		ok := scanner.Scan()
		if !ok {
			panic("scan error")
		}
		s.instruction = getInstructionFromString(scanner.Text())
		ok = scanner.Scan()
		if !ok {
			panic("scan error")
		}
		s.after = getStatusFromString(scanner.Text())
		samples = append(samples, s)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return samples
}

func addr(input []int, registry []int) {
	registry[input[3]] = registry[input[1]] + registry[input[2]]
}
func addi(input []int, registry []int) {
	registry[input[3]] = registry[input[1]] + input[2]
}

func mulr(input []int, registry []int) {
	registry[input[3]] = registry[input[1]] * registry[input[2]]
}
func muli(input []int, registry []int) {
	registry[input[3]] = registry[input[1]] * input[2]
}

func banr(input []int, registry []int) {
	registry[input[3]] = registry[input[1]] & registry[input[2]]
}
func bani(input []int, registry []int) {
	registry[input[3]] = registry[input[1]] & input[2]
}

func borr(input []int, registry []int) {
	registry[input[3]] = registry[input[1]] | registry[input[2]]
}
func bori(input []int, registry []int) {
	registry[input[3]] = registry[input[1]] | input[2]
}

func setr(input []int, registry []int) {
	registry[input[3]] = registry[input[1]]
}
func seti(input []int, registry []int) {
	registry[input[3]] = input[1]
}

func gtir(input []int, registry []int) {
	if input[1] > registry[input[2]] {
		registry[input[3]] = 1
	} else {
		registry[input[3]] = 0
	}
}
func gtri(input []int, registry []int) {
	if registry[input[1]] > input[2] {
		registry[input[3]] = 1
	} else {
		registry[input[3]] = 0
	}
}
func gtrr(input []int, registry []int) {
	if registry[input[1]] > registry[input[2]] {
		registry[input[3]] = 1
	} else {
		registry[input[3]] = 0
	}
}

func eqir(input []int, registry []int) {
	if input[1] == registry[input[2]] {
		registry[input[3]] = 1
	} else {
		registry[input[3]] = 0
	}
}
func eqri(input []int, registry []int) {
	if registry[input[1]] == input[2] {
		registry[input[3]] = 1
	} else {
		registry[input[3]] = 0
	}
}
func eqrr(input []int, registry []int) {
	if registry[input[1]] == registry[input[2]] {
		registry[input[3]] = 1
	} else {
		registry[input[3]] = 0
	}
}

var opcodes = []interface{}{
	addr,
	addi,
	mulr,
	muli,
	banr,
	bani,
	borr,
	bori,
	setr,
	seti,
	gtir,
	gtri,
	gtrr,
	eqir,
	eqri,
	eqrr}

var opcodesNames = []string{
	"addr",
	"addi",
	"mulr",
	"muli",
	"banr",
	"bani",
	"borr",
	"bori",
	"setr",
	"seti",
	"gtir",
	"gtri",
	"gtrr",
	"eqir",
	"eqri",
	"eqrr"}

func equal(a, b []int) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}

func getOpcodes(samples []sample) map[int][]int {
	codes := map[int][]int{}
	for i, s := range samples {
		fmt.Printf("%d sample %+v\n", i, s)
		for j, opcode := range opcodes {
			//opCodeName := opcodesNames[j]
			// fmt.Printf("opcode name: %s\n", opCodeName)

			registry := make([]int, len(s.before))
			copy(registry, s.before)

			// fmt.Printf("before     : %v\n", registry)
			// fmt.Printf("instruction: %v\n", s.instruction)
			opcode.(func([]int, []int))(s.instruction, registry)
			// fmt.Printf("after act: %v\n", registry)
			// fmt.Printf("after exp: %v\n", s.after)

			if equal(registry, s.after) {
				codes[i] = append(codes[i], j)
			}
		}
	}
	return codes
}

func getProgramm(filename string) [][]int {
	programm := [][]int{}
	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		if len(scanner.Text()) == 0 {
			continue
		}
		instr := getInstructionFromString(scanner.Text())
		programm = append(programm, instr)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return programm
}

func getKnownCodes(codesMap map[int][]int, samples []sample) map[int]int {
	knownCodes := map[int]bool{}
	sampleCodeToListMap := map[int]int{}
	for j := 0; j < 100; j++ {
		if len(codesMap) == 0 {
			break
		}

		for i, codes := range codesMap {
			if len(codes) == 1 {
				codeFromSample := samples[i].instruction[0]
				if knownCodes[codes[0]] {
					continue
				}
				knownCodes[codes[0]] = true
				sampleCodeToListMap[codeFromSample] = codes[0]
				fmt.Printf("knownCodesValues: %d, %s\n", codes[0], opcodesNames[codes[0]])
			}
		}

		//remove known codes
		newCodesMap := map[int][]int{}
		for i, codes := range codesMap {
			newCodes := []int{}
			for _, c := range codes {
				if !knownCodes[c] {
					newCodes = append(newCodes, c)
				}
			}
			if len(newCodes) > 0 {
				newCodesMap[i] = newCodes
			}
		}
		codesMap = newCodesMap
	}
	return sampleCodeToListMap
}

func executeProgramm(programm [][]int, knownCodes map[int]int) []int {
	registry := []int{0, 0, 0, 0}
	for _, instr := range programm {
		opcode := opcodes[knownCodes[instr[0]]]
		opcode.(func([]int, []int))(instr, registry)
	}
	return registry
}

func main() {

	samples := getData("input_block1.txt")
	fmt.Printf("%v", samples)

	codesMap := getOpcodes(samples)
	count := 0
	for _, codes := range codesMap {
		if len(codes) >= 3 {
			count++
		}
	}
	fmt.Printf("count: %d", count)
	//part1  - 542

	knownCodes := getKnownCodes(codesMap, samples)

	p := getProgramm("input_block2.txt")
	r := executeProgramm(p, knownCodes)
	fmt.Printf("registry: %v", r)

	// part2 - 575
}
