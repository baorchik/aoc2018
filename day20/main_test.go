package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestKnownData1(t *testing.T) {
	input := "^WNE$"
	expectedmap := `#####
#.|.#
#-###
#.|X#
#####
`

	// Act
	actualmap, doors := buildMap(input)
	fmt.Println(actualmap)
	assert.Equal(t, 3, doors)
	assert.Equal(t, expectedmap, actualmap)
}

func TestKnownData2(t *testing.T) {
	input := "^(W(NE|SE)|S)$"
	expectedmap := `#####
#.|.#
#-###
#.|X#
#-#-#
#.|.#
#####
`

	// Act
	actualmap, doors := buildMap(input)
	fmt.Println(actualmap)
	assert.Equal(t, 3, doors)
	assert.Equal(t, expectedmap, actualmap)
}

func TestKnownData3(t *testing.T) {
	input := "^ENWWW(NEEE|SSE(EE|N))$"
	expectedmap := `#########
#.|.|.|.#
#-#######
#.|.|.|.#
#-#####-#
#.#.#X|.#
#-#-#####
#.|.|.|.#
#########
`

	// Act
	actualmap, doors := buildMap(input)
	fmt.Println(actualmap)
	assert.Equal(t, 10, doors)
	assert.Equal(t, expectedmap, actualmap)
}

func TestKnownData4(t *testing.T) {
	input := "^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$"
	expectedmap := `###########
#.|.#.|.#.#
#-###-#-#-#
#.|.|.#.#.#
#-#####-#-#
#.#.#X|.#.#
#-#-#####-#
#.#.|.|.|.#
#-###-###-#
#.|.|.#.|.#
###########
`

	// Act
	actualmap, doors := buildMap(input)
	fmt.Println(actualmap)
	assert.Equal(t, 18, doors)
	assert.Equal(t, expectedmap, actualmap)
}

func TestKnownData5(t *testing.T) {
	input := "^ESSWWN(E|NNENN(EESS(WNSE|)SSS|WWWSSSSE(SW|NNNE)))$"
	expectedmap := `#############
#.|.|.|.|.|.#
#-#####-###-#
#.#.|.#.#.#.#
#-#-###-#-#-#
#.#.#.|.#.|.#
#-#-#-#####-#
#.#.#.#X|.#.#
#-#-#-###-#-#
#.|.#.|.#.#.#
###-#-###-#-#
#.|.#.|.|.#.#
#############
`

	// Act
	actualmap, doors := buildMap(input)
	fmt.Println(actualmap)
	assert.Equal(t, 23, doors)
	assert.Equal(t, expectedmap, actualmap)
}

func TestKnownData6(t *testing.T) {
	input := "^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))$"
	expectedmap := `###############
#.|.|.|.#.|.|.#
#-###-###-#-#-#
#.|.#.|.|.#.#.#
#-#########-#-#
#.#.|.|.|.|.#.#
#-#-#########-#
#.#.#.|X#.|.#.#
###-#-###-#-#-#
#.|.#.#.|.#.|.#
#-###-#####-###
#.|.#.|.|.#.#.#
#-#-#####-#-#-#
#.#.|.|.|.#.|.#
###############
`

	// Act
	actualmap, doors := buildMap(input)
	fmt.Println(actualmap)
	assert.Equal(t, 31, doors)
	assert.Equal(t, expectedmap, actualmap)
}
