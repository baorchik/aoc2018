package main

import (
	"bufio"
	"log"
	"os"
	"sort"
	"strings"
	"time"
)

func getPolymer(filename string) string {
	var result string
	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		result = scanner.Text()
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return result
}

func reactUnits(polymer string) (int, string) {
	var removedCount int
	resultString := ""
	for i := 0; i < len(polymer); i++ {
		if i < len(polymer)-1 &&
			polymer[i] != polymer[i+1] &&
			strings.ToLower(string(polymer[i])) == strings.ToLower(string(polymer[i+1])) {
			i++
			removedCount++
			continue
		}
		resultString += string(polymer[i])
	}
	//log.Printf("removedCount: %d, resultString: %s", removedCount, resultString)
	return removedCount, resultString
}

func reactAllUnits(polymer string) string {
	for {
		c, newResult := reactUnits(polymer)
		if c == 0 {
			return newResult
		}
		polymer = newResult
	}
}

func getAllDifferentUnits(polymer string) map[string]bool {
	units := map[string]bool{}
	for i := 0; i < len(polymer); i++ {
		units[strings.ToLower(string(polymer[i]))] = true
	}
	return units
}

func getReactionPerUnit(units map[string]bool, polymer string) map[string]int {
	reactionPerUnit := map[string]int{}
	for u := range units {
		newPolimer := strings.Replace(polymer, u, "", -1)
		newPolimer = strings.Replace(newPolimer, strings.ToUpper(u), "", -1)
		reactionPerUnit[u] = len(reactAllUnits(newPolimer))
		log.Printf("reactionPerUnit %s : %v", u, reactionPerUnit[u])

	}
	return reactionPerUnit
}

func main() {
	//polymer := getPolymer("input_example.txt")
	polymer := getPolymer("input.txt")

	//part 1
	polymer1 := reactAllUnits(polymer)
	log.Printf("polymer1 units: %v", len(polymer1))

	start := time.Now()
	//part 2
	units := getAllDifferentUnits(polymer)
	log.Printf("units: %v", units)
	reactionPerUnit := getReactionPerUnit(units, polymer)
	//log.Printf("reactionPerUnit: %v", reactionPerUnit)

	results := []int{}
	for _, v := range reactionPerUnit {
		results = append(results, v)
	}
	sort.Ints(results)

	log.Printf("reactionPerUnitMin: %v", results[0])

	elapsed := time.Since(start)
	log.Printf("Excecution took %s", elapsed)

	//log.Printf("reactionPerUnitMin: %v", results[len(results)-1])
}
