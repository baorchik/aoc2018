package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestKnownData1(t *testing.T) {
	target := tpoint{x: 10, y: 10}
	// 	expectedfield := `M=.|=.|.|=.
	// .|=|=|||..|
	// .==|....||=
	// =.|....|.==
	// =|..==...=.
	// =||.=.=||=|
	// |.=.===|||.
	// |..==||=.|=
	// .=..===..=|
	// .======|||=
	// .===|=|===T
	// `

	// Act
	f := fillRegions(510, &target)
	fprint := printField(f, &target)
	risk := getRisk(f, target)

	fmt.Println(fprint)
	//	assert.Equal(t, expectedfield, fprint)
	assert.Equal(t, 114, risk)
}

func TestKnownData2FindPath(t *testing.T) {
	target := tpoint{x: 4, y: 2}
	// 	expectedfield := `
	// M=.|=.|.|=.
	// .|=|=|||..|
	// .==|T...||=
	// =.|....|.==
	// =|..==...=.
	// `

	// Act
	f := fillRegions(510, &target)
	mins := findPath(f, &target)

	fmt.Printf("mins: %d\n", mins)
	assert.Equal(t, 20, mins)

	//[(3,2) tool 2, mins 19] [(3,1) tool 2, mins 18] [(3,0) tool 2, mins 17]
	// [(2,0) tool 3, mins 9] [(1,0) tool 3, mins 8] [(0,0) tool 2, mins 0]
}

func TestKnownData3FindPath(t *testing.T) {
	target := tpoint{x: 7, y: 2}
	// 	expectedfield := `
	// M=.|=.|.|=.
	// .|=|=|||..|
	// .==|...T||=
	// =.|....|.==
	// =|..==...=.
	// `

	// Act
	f := fillRegions(510, &target)
	mins := findPath(f, &target)

	fmt.Printf("mins: %d\n", mins)
	assert.Equal(t, 23, mins)

	//[(6,2) tool 2, mins 22] [(5,2) tool 2, mins 21] [(4,2) tool 2, mins 20]
	// [(3,2) tool 2, mins 19] [(3,1) tool 2, mins 18] [(3,0) tool 2, mins 17]
	// [(2,0) tool 3, mins 9] [(1,0) tool 3, mins 8] [(0,0) tool 2, mins 0]
}

func TestKnownData4FindPath(t *testing.T) {
	target := tpoint{x: 10, y: 10}
	// 	expectedfield := `
	// M=.|=.|.|=.
	// .|=|=|||..|
	// .==|...T||=
	// =.|....|.==
	// =|..==...=.
	// `

	// Act
	f := fillRegions(510, &target)
	fprint := printField(f, &target)
	mins := findPath(f, &target)

	fmt.Println(fprint)
	fmt.Printf("mins: %d\n", mins)
	assert.Equal(t, 45, mins)

	//[(6,2) tool 2, mins 22] [(5,2) tool 2, mins 21] [(4,2) tool 2, mins 20]
	// [(3,2) tool 2, mins 19] [(3,1) tool 2, mins 18] [(3,0) tool 2, mins 17]
	// [(2,0) tool 3, mins 9] [(1,0) tool 3, mins 8] [(0,0) tool 2, mins 0]
}
