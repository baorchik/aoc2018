package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
	"strings"
)

func getData(filename string) []int {
	var strData string
	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	scanner.Scan()
	strData = scanner.Text()

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	if len(strData) == 0 {
		log.Fatal("no data!")
	}

	values := strings.Split(strData, " ")
	res := []int{}
	for _, i := range values {
		v, err := strconv.Atoi(i)
		if err != nil {
			log.Fatal(err)
		}
		res = append(res, v)
	}

	return res
}

type valueNode struct {
	children valueNodes
	meta     []int
}
type nodes [][]int
type valueNodes []valueNode

func getChildNodes(idx int, data []int) (int, nodes) {
	subNodes := nodes{}
	childrenSize := data[idx]
	idx++
	metadataSize := data[idx]
	idx++

	log.Printf("idx: %d, childrenSize: %d, metadataSize: %d", idx, childrenSize, metadataSize)
	for i := 0; i < childrenSize; i++ {
		newIdx, chNodes := getChildNodes(idx, data)
		subNodes = append(subNodes, chNodes...)
		idx = newIdx
		log.Printf("idx: %d, chNodes: %v", idx, chNodes)
	}

	metadata := []int{}
	if metadataSize != 0 {
		for i := 0; i < metadataSize; i++ {
			metadata = append(metadata, data[idx])
			idx++
		}
		log.Printf("idx: %d, metadata: %v", idx, metadata)
	}
	nodes := nodes{metadata}
	nodes = append(nodes, subNodes...)

	return idx, nodes
}

func getValuedNode(idx int, data []int) (int, valueNode) {
	childrenSize := data[idx]
	idx++

	currentNode := valueNode{}
	currentNode.children = valueNodes{}
	currentNode.meta = []int{}

	metadataSize := data[idx]
	idx++

	log.Printf("idx: %d, childrenSize: %d, metadataSize: %d", idx, childrenSize, metadataSize)
	for i := 0; i < childrenSize; i++ {
		newIdx, chNode := getValuedNode(idx, data)
		currentNode.children = append(currentNode.children, chNode)
		idx = newIdx
		log.Printf("idx: %d, chNode: %v", idx, chNode)
	}

	if metadataSize != 0 {
		for i := 0; i < metadataSize; i++ {
			currentNode.meta = append(currentNode.meta, data[idx])
			idx++
		}
		log.Printf("idx: %d, metadata: %v", idx, currentNode.meta)
	}

	return idx, currentNode
}

func printSum1(n nodes) {
	totalSum := 0
	for i := 0; i < len(n); i++ {
		for j := 0; j < len(n[i]); j++ {
			totalSum += n[i][j]
		}
	}
	log.Printf("total sum: %d", totalSum)
}

func getValue(n valueNode) int {
	log.Printf("getValue for : %+v", n)
	metadataSum := 0
	if len(n.children) == 0 {
		for i := 0; i < len(n.meta); i++ {
			metadataSum += n.meta[i]
		}
		log.Printf("metadataSum: %d", metadataSum)
		return metadataSum
	}

	for i := 0; i < len(n.meta); i++ {
		chidIdx := n.meta[i] - 1
		if chidIdx >= len(n.children) {
			continue
		}

		log.Printf("get valie for child: %d", chidIdx)
		metadataSum += getValue(n.children[chidIdx])
	}
	log.Printf("metadataSum: %d", metadataSum)
	return metadataSum
}

func main() {
	//data := getData("input_example.txt")
	data := getData("input.txt")
	log.Printf("data: %v", data)
	//_, nodes := getChildNodes(0, data)
	//log.Printf("nodes result: %v", nodes)
	//printSum1(nodes)
	//part1 - 45194

	_, vNode := getValuedNode(0, data)
	log.Printf("vNode: %+v", vNode)
	val := getValue(vNode)

	log.Printf("total value: %d", val)
}
