package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestKnownValues(t *testing.T) {
	receipts := []int{3, 7}
	e1 := 0
	e2 := 1
	printStatus(receipts, e1, e2)

	assert.Equal(t, "5158916779", makeRecepies(receipts, e1, e2, 9))
	assert.Equal(t, "0124515891", makeRecepies(receipts, e1, e2, 5))
	assert.Equal(t, "9251071085", makeRecepies(receipts, e1, e2, 18))
	assert.Equal(t, "5941429882", makeRecepies(receipts, e1, e2, 2018))
}

func TestKnownValues2(t *testing.T) {
	receipts := []int{3, 7}
	e1 := 0
	e2 := 1
	printStatus(receipts, e1, e2)

	assert.Equal(t, 9, makeRecepies2(receipts, e1, e2, "51589"))
	assert.Equal(t, 5, makeRecepies2(receipts, e1, e2, "01245"))
	assert.Equal(t, 18, makeRecepies2(receipts, e1, e2, "92510"))
	assert.Equal(t, 2018, makeRecepies2(receipts, e1, e2, "59414"))
}
