package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

type tInstruction struct {
	opname string
	op     interface{}
	args   tArgs
}

type tArgs []int
type tRegistry []int64
type tInstructions map[int]tInstruction

func getIpFromString(str string) int {
	log.Println(str)
	var v int
	_, err := fmt.Sscanf(str, "#ip %d", &v)
	if err != nil {
		log.Fatal(err)
	}
	return v
}

func getInstructionFromString(str string) tInstruction {
	log.Println(str)
	var a, b, c int
	var inst string
	_, err := fmt.Sscanf(str, "%s %d %d %d", &inst, &a, &b, &c)
	if err != nil {
		log.Fatal(err)
	}

	opId := -1
	for i, op := range opcodesNames {
		if op == inst {
			opId = i
			break
		}
	}
	if opId == -1 {
		panic(fmt.Sprintf("unmapped instruction %s", inst))
	}

	return tInstruction{
		opname: inst,
		op:     opcodes[opId],
		args:   tArgs{0, a, b, c}}
}

func getData(filename string) (tInstructions, int) {
	instructions := tInstructions{}
	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)

	ok := scanner.Scan()
	if !ok {
		panic("scan error")
	}
	ip := getIpFromString(scanner.Text())
	i := 0
	for scanner.Scan() {
		if len(scanner.Text()) == 0 {
			continue
		}
		instructions[i] = getInstructionFromString(scanner.Text())
		i++
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return instructions, ip
}

func addr(input tArgs, registry tRegistry) {
	registry[input[3]] = registry[input[1]] + registry[input[2]]
}
func addi(input tArgs, registry tRegistry) {
	registry[input[3]] = registry[input[1]] + int64(input[2])
}

func mulr(input tArgs, registry tRegistry) {
	registry[input[3]] = registry[input[1]] * registry[input[2]]
}
func muli(input tArgs, registry tRegistry) {
	registry[input[3]] = registry[input[1]] * int64(input[2])
}

func setr(input tArgs, registry tRegistry) {
	registry[input[3]] = registry[input[1]]
}
func seti(input tArgs, registry tRegistry) {
	registry[input[3]] = int64(input[1])
}

func gtrr(input tArgs, registry tRegistry) {
	if registry[input[1]] > registry[input[2]] {
		registry[input[3]] = 1
	} else {
		registry[input[3]] = 0
	}
}

func eqrr(input tArgs, registry tRegistry) {
	if registry[input[1]] == registry[input[2]] {
		registry[input[3]] = 1
	} else {
		registry[input[3]] = 0
	}
}

var opcodes = []interface{}{
	addr,
	addi,
	mulr,
	muli,
	setr,
	seti,
	gtrr,
	eqrr}

var opcodesNames = []string{
	"addr",
	"addi",
	"mulr",
	"muli",
	"setr",
	"seti",
	"gtrr",
	"eqrr"}

func doIteration(instructions tInstructions, ip int, registry tRegistry) bool {
	pointer := int(registry[ip])
	instr, ok := instructions[pointer]
	if !ok {
		fmt.Println("stop!")
		return true
	}

	//fmt.Printf("ip=%d\n", pointer)
	//fmt.Printf("ip=%02d %03v ", pointer, registry)
	//fmt.Printf("%s %02d %02d %02d ", instr.opname, instr.arg[1], instr.arg[2], instr.arg[3])
	instr.op.(func(tArgs, tRegistry))(instr.args, registry)
	//fmt.Printf("%03v\n", registry)
	registry[ip]++
	return false
}

func equal(a, b []int) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}

// func wrapperGtrr132(registry tRegistry) {
// 	if registry[1] <= registry[3] {
// 		fmt.Println("Gtrr132-1")
// 		registry[2] = registry[5] * registry[1]
// 		wrapperEqrr232(registry)
// 	} else {
// 		fmt.Println("Gtrr132-2")

// 		// ip=09 [1374 931 846300 930 009 910] gtrr 01 03 02 [1374 931 001 930 009 910]
// 		// ip=10 [1374 931 001 930 010 910] addr 04 02 04 [1374 931 001 930 011 910]
// 		// ip=12 [1374 931 001 930 012 910] addi 05 01 05 [1374 931 001 930 012 911]
// 		registry[2] = 1
// 		registry[5]++
// 		registry[4] = 13

// 	}
// }

func wrapperEqrr232(registry tRegistry) {
	r0 := registry[0]
	r3 := registry[3]
	r5 := registry[5]

	for i := r5; i <= r3; i++ {
		for j := int64(1); j <= r3; j++ {
			if i*j == r3 {
				r0 += i
			}
			if i*j > r3 {
				break
			}
		}
	}

	registry[0] = r0
	registry[1] = 1
	registry[2] = r3
	registry[3] = r3
	registry[4] = 13
	registry[5] = r3 + 1

}

// func wrapperGtrr532(registry tRegistry) {
// 	// ip=13 [1374 931 001 930 013 927] gtrr 05 03 02 [1374 931 000 930 013 927]
// 	// ip=14 [1374 931 000 930 014 927] addr 02 04 04 [1374 931 000 930 014 927]
// 	// ip=15 [1374 931 000 930 015 927] seti 01 08 04 [1374 931 000 930 001 927]
// 	// ip=02 [1374 931 000 930 002 927] seti 01 01 01 [1374 001 000 930 002 927]

// 	registry[2] = registry[5]
// 	registry[1] = 1
// 	wrapperEqrr232(registry)

// }

func doIteration2(instructions tInstructions, ip int, registry tRegistry) bool {
	pointer := int(registry[ip])
	instr, ok := instructions[pointer]
	if !ok {
		fmt.Println("stop!")
		return true
	}

	// if pointer == 9 {
	// 	wrapperGtrr132(registry)
	// 	return false
	// }

	if pointer == 4 {
		wrapperEqrr232(registry)
		//fmt.Printf("%03v\n", registry)
		return false
	}

	// if pointer == 13 && registry[5] <= registry[3] {
	// 	wrapperGtrr532(registry)
	// 	//fmt.Println("Gtrr532")
	// 	// fmt.Println()
	// 	return false
	// }

	fmt.Printf("ip=%02d %03v ", pointer, registry)
	instr.op.(func(tArgs, tRegistry))(instr.args, registry)

	fmt.Printf("%s %02d %02d %02d ", instr.opname, instr.args[1], instr.args[2], instr.args[3])
	fmt.Printf("%03v\n", registry)
	registry[ip]++
	return false
}

func doExecuteInstructions(instructions tInstructions, ip int) {
	//part1 - 2304
	//registry := tRegistry{0, 0, 0, 0, 0, 0}
	registry := tRegistry{1, 0, 0, 0, 0, 0}

	//part2
	// 10551330 is too low

	var stop bool
	for j := 0; j >= 0; j++ {
		stop = doIteration2(instructions, ip, registry)
		if stop {
			fmt.Printf("j: %d\n", j)
			break
		}
		// if int(registry[ip]) == 9 {
		// 	fmt.Println()
		// if int(registry[ip]) == 13 {
		// 	fmt.Printf("j: %d, %03v\n", j, registry)
		// }

		if j%100000000 == 0 {
			fmt.Printf("j: %d, %03v\n", j, registry)
		}
	}

	// i := 0
	// for j := 0; j < 0; j++ {
	// 	stop = doIteration2(instructions, ip, registry)
	// 	if stop {
	// 		break
	// 	}

	// 	if i%100000000 == 0 {
	// 		fmt.Printf("i: %d, %03v\n", i, registry)
	// 	}
	// 	i++
	// }
	fmt.Printf("%03v\n", registry)
	fmt.Printf("value at reg0: %d\n", registry[0])
}

func main() {

	//instructions, ip := getData("input_example.txt")
	instructions, ip := getData("input.txt")
	fmt.Printf("%v\n", instructions)
	fmt.Printf("ip: %d\n", ip)
	doExecuteInstructions(instructions, ip)

	//part2 - 28137600
}
