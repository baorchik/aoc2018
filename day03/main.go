package main

import (
	"bufio"
	"log"
	"os"
	"regexp"
	"strconv"
)

type claim struct {
	n int64
	x int64
	y int64
	w int64
	h int64
}

func lineToClaim(str string) claim {
	r, err := regexp.Compile("#(\\d+)\\s*@\\s*(\\d+),(\\d+):\\s*(\\d+)x(\\d+)")
	if err != nil {
		log.Fatal(err)
	}
	res := r.FindStringSubmatch(str)
	c := claim{}

	c.n, err = strconv.ParseInt(res[1], 10, 32)
	if err != nil {
		log.Fatal(err)
	}
	c.x, err = strconv.ParseInt(res[2], 10, 32)
	if err != nil {
		log.Fatal(err)
	}
	c.y, err = strconv.ParseInt(res[3], 10, 32)
	if err != nil {
		log.Fatal(err)
	}
	c.w, err = strconv.ParseInt(res[4], 10, 32)
	if err != nil {
		log.Fatal(err)
	}
	c.h, err = strconv.ParseInt(res[5], 10, 32)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("claim: %+v", c)
	return c
}

func getClaims(filename string) []claim {
	claims := []claim{}

	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {

		claims = append(claims, lineToClaim(scanner.Text()))
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return claims
}

const sheetSize = 1000

func putClaimsOnTheSheet(claims []claim) {
	sheet := [sheetSize][sheetSize]int{}

	for _, c := range claims {
		for i := c.x; i < c.x+c.w; i++ {
			for j := c.y; j < c.y+c.h; j++ {
				sheet[i][j]++
			}
		}
	}

	//log.Printf("sheet: %+v\n", sheet)

	var sharedSize int64
	for i := 0; i < sheetSize; i++ {
		for j := 0; j < sheetSize; j++ {
			if sheet[i][j] > 1 {
				sharedSize++
			}
		}
	}
	log.Printf("sharedSize: %+v\n", sharedSize)
	for _, c := range claims {
		overlap := false
		for i := c.x; i < c.x+c.w; i++ {
			for j := c.y; j < c.y+c.h; j++ {
				if sheet[i][j] > 1 {
					overlap = true
				}
			}
		}
		if !overlap {
			log.Printf("claime %+v doesn't overlap", c.n)
		}
	}
}

func main() {
	//claims := getClaims("input_example.txt")
	claims := getClaims("input.txt")
	putClaimsOnTheSheet(claims)
}
