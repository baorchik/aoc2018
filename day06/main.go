package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
)

type coordinate struct {
	id int
	x  int
	y  int
}

type cell struct {
	owner     int
	distances map[int]int
	isBorder  bool
	isSafe    bool
}

type matrix [][]cell

var bottomRight coordinate

func getCoordinates(filename string) []coordinate {
	coordinates := []coordinate{}

	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	var x, y, id int
	for scanner.Scan() {
		count, err := fmt.Sscanf(scanner.Text(), "%d, %d", &x, &y)
		if err != nil {
			log.Printf("str: '%s'", scanner.Text())
			log.Fatal(err)
		}
		if count == 0 {
			log.Fatal("0 count!")
		}
		id++
		coordinates = append(coordinates, coordinate{id: id, x: x, y: y})
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return coordinates
}

func getBottomRight(coords []coordinate) coordinate {
	bottom := coordinate{}

	for _, c := range coords {
		if c.x > bottom.x {
			bottom.x = c.x
		}
		if c.y > bottom.y {
			bottom.y = c.y
		}
	}

	return coordinate{x: bottom.x + 1, y: bottom.y + 1}
}

func getDistance(x1, y1, x2, y2 float64) float64 {
	val := math.Abs(x1-x2) + math.Abs(y1-y2)
	return val
}

func getMatrix(coords []coordinate) matrix {

	m := make(matrix, bottomRight.x+1)

	for i := 0; i <= bottomRight.x; i++ {
		m[i] = make([]cell, bottomRight.y+1)
		for j := 0; j <= bottomRight.y; j++ {
			m[i][j] = cell{owner: 0, distances: map[int]int{}, isBorder: false}
		}
	}

	for _, c := range coords {
		m[c.x][c.y] = cell{owner: c.id, distances: map[int]int{c.id: 0}, isBorder: false}
	}

	return m
}

func setDistances(m matrix, coords []coordinate) {
	for i := 0; i <= bottomRight.x; i++ {
		for j := 0; j <= bottomRight.y; j++ {
			for _, c := range coords {
				d := getDistance(float64(i), float64(j), float64(c.x), float64(c.y))
				m[i][j].distances[c.id] = int(d)
			}
		}
	}
}

func setOwnersAndBorders(m matrix) {
	for i := 0; i <= bottomRight.x; i++ {
		for j := 0; j <= bottomRight.y; j++ {
			if m[i][j].owner != 0 {
				continue
			}

			uniqueDist := map[int]bool{}
			minDist := 1000
			owner := 0
			isBorder := false
			distSum := 0
			for id, dist := range m[i][j].distances {
				distSum += dist
				if dist <= minDist {
					minDist = dist
					owner = id
					isBorder = uniqueDist[dist]
				}
				uniqueDist[dist] = true
			}
			m[i][j].owner = owner
			m[i][j].isBorder = isBorder
			m[i][j].isSafe = distSum < 32
		}
	}
}

func setSafeAreas(m matrix, lim int) {
	for i := 0; i <= bottomRight.x; i++ {
		for j := 0; j <= bottomRight.y; j++ {
			distSum := 0
			for _, dist := range m[i][j].distances {
				distSum += dist
			}
			m[i][j].isSafe = distSum < lim
		}
	}
}

func printMatrix(m matrix) {
	log.Println("================")
	for i := 0; i <= bottomRight.x; i++ {
		line := ""
		for j := 0; j <= bottomRight.y; j++ {
			if m[i][j].owner == 0 {
				line += "++ "
			} else if m[i][j].isBorder {
				line += ".. "
			} else {
				line += fmt.Sprintf("%02v ", m[i][j].owner)
			}
		}
		log.Println(line)
	}
}

func printSafeArea(m matrix) {
	log.Println("================")
	for i := 0; i <= bottomRight.x; i++ {
		line := ""
		for j := 0; j <= bottomRight.y; j++ {
			if m[i][j].isSafe {
				line += "## "
			} else {
				line += ".. "
			}
		}
		log.Println(line)
	}
}

func printSafeAreaSize(m matrix) {
	totalSafeSize := 0
	for i := 0; i <= bottomRight.x; i++ {
		for j := 0; j <= bottomRight.y; j++ {
			if m[i][j].isSafe {
				totalSafeSize++
			}
		}
	}
	log.Printf("totalSafeSize: %d", totalSafeSize)
}
func printMaxArea(m matrix) {
	areas := map[int]int{}
	infinitiveAreas := map[int]bool{}
	for i := 0; i <= bottomRight.x; i++ {
		for j := 0; j <= bottomRight.y; j++ {
			c := m[i][j]
			id := c.owner
			if id != 0 && !c.isBorder {
				areas[id]++
				if i == 0 || j == 0 || i == bottomRight.x || j == bottomRight.y {
					infinitiveAreas[id] = true
				}
			}
		}
	}
	log.Printf("areas: %v\n", areas)
	log.Printf("infinitiveAreas: %v\n", infinitiveAreas)

	finiteAreas := map[int]int{}
	maxV := 0
	for a, v := range areas {
		if !infinitiveAreas[a] {
			finiteAreas[a] = v
			if v > maxV {
				maxV = v
			}
		}
	}
	log.Printf("finiteAreas: %v\n", finiteAreas)
	log.Printf("maxV: %v\n", maxV)
}

func main() {
	//coordinates := getCoordinates("input_example.txt")
	coordinates := getCoordinates("input.txt")
	log.Printf("coordinates: %v", coordinates)
	bottomRight = getBottomRight(coordinates)
	log.Printf("bottomRight: %v", bottomRight)

	m := getMatrix(coordinates)
	setDistances(m, coordinates)
	//part1 //setOwnersAndBorders(m)
	//printMatrix(m)

	//part1 printMaxArea(m)
	//part1 maxV: 3569

	//part2
	//setSafeAreas(m, 32)
	setSafeAreas(m, 10000)
	//printSafeArea(m)
	printSafeAreaSize(m)
	//48978
}
