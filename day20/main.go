package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strings"
)

type tPoint struct {
	x int
	y int
}

type tPathPoint struct {
	x    int
	y    int
	prev *tPathPoint
}

type tRoomsVisitedGrid map[tPoint]bool

type tRoomsPrintRow map[int]string
type tRoomsPrintGrid map[int]tRoomsPrintRow

type tRoomsNext map[string]*tRoom
type tRoomsRow map[int]*tRoom
type tRoomsGrid map[int]tRoomsRow
type tRoom struct {
	next tRoomsNext
}

func getData(filename string) string {
	var instruction string
	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		instruction = scanner.Text()
	}

	return instruction
}

func addRoom(x, y, nx, ny int, direction string, rooms tRoomsGrid) {

	var oppositeDirection string
	switch direction {
	case "n":
		oppositeDirection = "s"
	case "e":
		oppositeDirection = "w"
	case "w":
		oppositeDirection = "e"
	case "s":
		oppositeDirection = "n"
	}

	if _, ok := rooms[ny]; !ok {
		rooms[ny] = tRoomsRow{}
	}

	if _, ok := rooms[ny][nx]; !ok {
		rooms[ny][nx] = &tRoom{
			next: tRoomsNext{}}
	}
	rooms[ny][nx].next[oppositeDirection] = rooms[y][x]
	rooms[y][x].next[direction] = rooms[ny][nx]
}

func buildBranch(instruction string, x, y, idx int, rooms tRoomsGrid) int {
	xBlock := x
	yBlock := y
	for i := idx; i < len(instruction); i++ {
		switch string(instruction[i]) {
		case "^", "$":
			continue
		case "N":
			ny := y - 1
			nx := x
			addRoom(x, y, nx, ny, "n", rooms)
			x = nx
			y = ny
		case "E":
			ny := y
			nx := x + 1
			addRoom(x, y, nx, ny, "e", rooms)
			x = nx
			y = ny
		case "W":
			ny := y
			nx := x - 1
			addRoom(x, y, nx, ny, "w", rooms)
			x = nx
			y = ny
		case "S":
			ny := y + 1
			nx := x
			addRoom(x, y, nx, ny, "s", rooms)
			x = nx
			y = ny
		case "(":
			i = buildBranch(instruction, x, y, i+1, rooms)
		case "|":
			x = xBlock
			y = yBlock
		case ")":
			return i
		}
	}
	return len(instruction)
}

func buildFacilities(instruction string) tRoomsGrid {
	rooms := tRoomsGrid{}
	rooms[0] = tRoomsRow{}
	rooms[0][0] = &tRoom{}
	rooms[0][0].next = tRoomsNext{}

	buildBranch(instruction, 0, 0, 0, rooms)
	return rooms
}

func writeToOutput(x, y int, str string, outputMap tRoomsPrintGrid) {
	if outputMap[y][x] == "" {
		outputMap[y][x] = str
	} else if outputMap[y][x] != str {
		panic(fmt.Sprintf("(%d,%d) rewriting %s with %s", y, x, outputMap[y][x], str))
	}
}

func printRoom(x, y int, rooms tRoomsGrid, outputMap tRoomsPrintGrid, visited tRoomsVisitedGrid) {
	room := rooms[y][x]

	visited[tPoint{x: x, y: y}] = true

	outputMap[2*y][2*x] = "."
	writeToOutput(2*x-1, 2*y-1, "#", outputMap)
	writeToOutput(2*x+1, 2*y+1, "#", outputMap)
	writeToOutput(2*x-1, 2*y+1, "#", outputMap)
	writeToOutput(2*x+1, 2*y-1, "#", outputMap)

	if _, ok := room.next["n"]; !ok {
		writeToOutput(2*x, 2*y-1, "#", outputMap)
	} else {
		writeToOutput(2*x, 2*y-1, "-", outputMap)
		if !visited[tPoint{x: x, y: y - 1}] {
			printRoom(x, y-1, rooms, outputMap, visited)
		}
	}
	if _, ok := room.next["e"]; !ok {
		writeToOutput(2*x+1, 2*y, "#", outputMap)
	} else {
		writeToOutput(2*x+1, 2*y, "|", outputMap)
		if !visited[tPoint{x: x + 1, y: y}] {
			printRoom(x+1, y, rooms, outputMap, visited)
		}
	}
	if _, ok := room.next["w"]; !ok {
		writeToOutput(2*x-1, 2*y, "#", outputMap)
	} else {
		writeToOutput(2*x-1, 2*y, "|", outputMap)
		if !visited[tPoint{x: x - 1, y: y}] {
			printRoom(x-1, y, rooms, outputMap, visited)
		}
	}
	if _, ok := room.next["s"]; !ok {
		writeToOutput(2*x, 2*y+1, "#", outputMap)
	} else {
		writeToOutput(2*x, 2*y+1, "-", outputMap)
		if !visited[tPoint{x: x, y: y + 1}] {
			printRoom(x, y+1, rooms, outputMap, visited)
		}
	}
}

func outputMapToString(outputMap tRoomsPrintGrid) string {
	keysY := []int{}
	for k := range outputMap {
		keysY = append(keysY, k)
	}
	sort.Ints(keysY)
	var output strings.Builder

	for _, y := range keysY {
		keysX := []int{}
		for k := range outputMap[y] {
			keysX = append(keysX, k)
		}
		sort.Ints(keysX)

		for _, x := range keysX {
			output.WriteString(outputMap[y][x])
		}
		output.WriteString("\n")
	}
	return output.String()
}

func getPointsToVisit(p *tPathPoint, rooms tRoomsGrid, visited tRoomsVisitedGrid) []*tPathPoint {
	pointsToVisit := []*tPathPoint{}
	room := rooms[p.y][p.x]
	if _, ok := room.next["n"]; ok && !visited[tPoint{x: p.x, y: p.y - 1}] {
		pointsToVisit = append(pointsToVisit, &tPathPoint{x: p.x, y: p.y - 1, prev: p})
	}
	if _, ok := room.next["e"]; ok && !visited[tPoint{x: p.x + 1, y: p.y}] {
		pointsToVisit = append(pointsToVisit, &tPathPoint{x: p.x + 1, y: p.y, prev: p})
	}
	if _, ok := room.next["w"]; ok && !visited[tPoint{x: p.x - 1, y: p.y}] {
		pointsToVisit = append(pointsToVisit, &tPathPoint{x: p.x - 1, y: p.y, prev: p})
	}
	if _, ok := room.next["s"]; ok && !visited[tPoint{x: p.x, y: p.y + 1}] {
		pointsToVisit = append(pointsToVisit, &tPathPoint{x: p.x, y: p.y + 1, prev: p})
	}
	return pointsToVisit
}

func searchPathToRoom(pTarget *tPoint, rooms tRoomsGrid) []*tPathPoint {

	startPoint := tPathPoint{
		x:    0,
		y:    0,
		prev: nil}

	visited := tRoomsVisitedGrid{}

	pointsToVisit := []*tPathPoint{}
	pointsToVisitNew := []*tPathPoint{&startPoint}

	var path []*tPathPoint
	for {
		if len(pointsToVisitNew) == 0 {
			panic("path not found!")
		}
		pointsToVisit = []*tPathPoint{}
		for _, v := range pointsToVisitNew {
			pointsToVisit = append(pointsToVisit, v)
		}
		pointsToVisitNew = []*tPathPoint{}

		for _, ptv := range pointsToVisit {
			visited[tPoint{x: ptv.x, y: ptv.y}] = true
			if ptv.x == pTarget.x && ptv.y == pTarget.y {
				for {
					if ptv == nil {
						return path
					}
					path = append(path, ptv)
					ptv = ptv.prev
				}
			}
			pointsToVisitNew = append(pointsToVisitNew, getPointsToVisit(ptv, rooms, visited)...)
		}
	}
}

func getPathLengthToRoom(x, y int, rooms tRoomsGrid) int {

	path := searchPathToRoom(&tPoint{x: x, y: y}, rooms)

	// fmt.Printf("path to (%02d,%02d) : ", x, y)
	// for _, v := range path {
	// 	fmt.Printf("(%02d,%02d)", v.x, v.y)
	// }
	// fmt.Printf("len: %d", len(path)-1)
	// fmt.Println()
	return len(path) - 1
}

func getFurtherstPath(rooms tRoomsGrid) (int, int) {
	pathLens := []int{}
	for y, row := range rooms {
		for x := range row {
			pathLens = append(pathLens, getPathLengthToRoom(x, y, rooms))
		}
	}

	sort.Ints(pathLens)

	roomsWith1000dorsPath := 0
	for _, l := range pathLens {
		if l >= 1000 {
			roomsWith1000dorsPath++
		}
	}
	return pathLens[len(pathLens)-1], roomsWith1000dorsPath
}

func buildMap(instruction string) (string, int, int) {
	rooms := buildFacilities(instruction)

	outputMap := tRoomsPrintGrid{}
	visitedPrint := tRoomsVisitedGrid{}

	for k := range rooms {
		outputMap[2*k-1] = tRoomsPrintRow{}
		outputMap[2*k] = tRoomsPrintRow{}
		outputMap[2*k+1] = tRoomsPrintRow{}
	}

	printRoom(0, 0, rooms, outputMap, visitedPrint)
	outputMap[0][0] = "X"

	furtherstPathLen := 0
	roomsWith1000dorsPathCount := 0
	furtherstPathLen, roomsWith1000dorsPathCount = getFurtherstPath(rooms)
	return outputMapToString(outputMap), furtherstPathLen, roomsWith1000dorsPathCount
}

// 2555 too low
func main() {
	instruction := getData("input.txt")
	actualmap, doors, roomsWith1000dorsPathCount := buildMap(instruction)
	fmt.Println(actualmap)
	fmt.Printf("doors: %d\n", doors)
	fmt.Printf("roomsWith1000dorsPathCount: %d\n", roomsWith1000dorsPathCount)

	//part1 - 3415
	//part2 - 8583
}
