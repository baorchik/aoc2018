package main

import (
	"bufio"
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"
)

type point struct {
	X  int
	Y  int
	vX int
	vY int
}

func getTestData(filename string) []point {
	data := []point{}
	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		p := point{}
		log.Println(scanner.Text())
		_, err := fmt.Sscanf(scanner.Text(),
			"position=<%d, %d> velocity=<%d, %d>",
			&p.X, &p.Y, &p.vX, &p.vY)
		if err != nil {
			log.Fatal(err)
		}
		data = append(data, p)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return data
}

func getMinPoint(data []point) point {
	minP := point{X: 999999, Y: 999999}
	for _, p := range data {
		if p.X < minP.X {
			minP.X = p.X
		}
		if p.Y < minP.Y {
			minP.Y = p.Y
		}
	}
	return minP
}

func getMaxPoint(data []point) point {
	maxP := point{X: 0, Y: 0}
	for _, p := range data {
		if p.X > maxP.X {
			maxP.X = p.X
		}
		if p.Y > maxP.Y {
			maxP.Y = p.X
		}
	}
	return maxP
}

var file *os.File

func printScreen(screen [][]string) {

	var err error
	if file == nil {
		t := time.Now()
		file, err = os.Create(fmt.Sprintf("%02d-%02d-%02d-%05d.txt",
			t.Hour(), t.Minute(), t.Second(), rand.Int()))
		if err != nil {
			log.Fatal(err)
		}
	}

	for i := 0; i < len(screen); i++ {
		for j := 0; j < len(screen[i]); j++ {
			file.WriteString(fmt.Sprintf("%s ", screen[i][j]))
		}
		file.WriteString(fmt.Sprintln())
	}

	file.WriteString(fmt.Sprintln("---------------"))
	file.WriteString(fmt.Sprintln("---------------"))
	file.WriteString(fmt.Sprintln("---------------"))

}

func shiftData(data []point, shiftX, shiftY int) []point {
	dataNew := []point{}
	for _, p := range data {
		dataNew = append(dataNew, point{
			X:  p.X + shiftX,
			Y:  p.Y + shiftY,
			vX: p.vX,
			vY: p.vY})
	}
	return dataNew
}

func doMove(data []point) []point {
	dataNew := []point{}
	for _, p := range data {
		dataNew = append(dataNew, point{
			X:  p.X + p.vX,
			Y:  p.Y + p.vY,
			vX: p.vX,
			vY: p.vY})
	}
	return dataNew
}

func abs(a int) int {
	if a < 0 {
		return -a
	}
	return a
}

func screenSize(data []point) int {
	minP := getMinPoint(data)
	maxP := getMaxPoint(data)

	return maxP.X + maxP.Y + abs(minP.X) + abs(minP.Y)
}

func printData(data []point) {
	minP := getMinPoint(data)
	data = shiftData(data, -minP.X, -minP.Y)
	maxP := getMaxPoint(data)

	screen := [][]string{}
	for i := 0; i <= maxP.X; i++ {
		screen = append(screen, []string{})
		for j := 0; j <= maxP.Y; j++ {
			screen[i] = append(screen[i], ". ")
		}
	}

	for _, p := range data {
		screen[p.X][p.Y] = "##"
	}
	printScreen(screen)

}

func main() {
	//data := getTestData("input_example.txt")
	data := getTestData("input.txt")
	log.Printf("data: %+v", data)

	minSize := 999999
	step := 0
	dataTemp := data
	for i := 0; i < 20000; i++ {
		s := screenSize(dataTemp)
		if s < minSize {
			minSize = s
			step = i
		}
		dataTemp = doMove(dataTemp)
	}
	log.Printf("minSize: %d, step: %d", minSize, step)

	offset := 20
	for i := 0; i < step-offset; i++ {
		data = doMove(data)
	}

	for i := 0; i < offset+2; i++ {
		printData(data)
		file.WriteString(fmt.Sprintf("offset: %d", step-offset+i+1))
		data = doMove(data)
	}
	defer file.Close()

	// GPJLLLLH
	// 10515

}
