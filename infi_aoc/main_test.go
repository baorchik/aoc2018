package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestKnownData1(t *testing.T) {
	f, lastp := getData("input_example.txt")
	printField(f, lastp)
	fmt.Printf("lastPoint %+v\n", lastp)
	steps := findPathStepsCount(f, lastp)

	assert.Equal(t, 6, steps)
}

func TestKnownData2(t *testing.T) {
	f, lastp := getData("input_example2.txt")
	printField(f, lastp)
	fmt.Printf("lastPoint %+v\n", lastp)
	steps := findPathStepsCount(f, lastp)

	assert.Equal(t, 12, steps)
}

func TestKnownData3Part1(t *testing.T) {
	f, lastp := getData("input.txt")
	printField(f, lastp)
	fmt.Printf("lastPoint %+v\n", lastp)
	steps := findPathStepsCount(f, lastp)

	assert.Equal(t, 42, steps)
}

func TestKnownData1Part2(t *testing.T) {
	f, lastp := getData("input_example1_p2.txt")
	printField(f, lastp)
	fmt.Printf("lastPoint %+v\n", lastp)
	steps := findPathStepsCountWithShift(f, lastp)

	assert.Equal(t, 4, steps)
}

func TestKnownData2Part2(t *testing.T) {
	f, lastp := getData("input_example2_p2.txt")
	printField(f, lastp)
	fmt.Printf("lastPoint %+v\n", lastp)
	steps := findPathStepsCountWithShift(f, lastp)

	assert.Equal(t, 4, steps)
}
