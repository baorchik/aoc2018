package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

const (
	open int = iota + 1
	trees
	lumberyard
)

const (
	openS       = "."
	treesS      = "|"
	lumberyardS = "#"
)

type tCell struct {
	kind int
}
type tField [][]tCell

func getRow(str string) []tCell {
	r := []tCell{}
	for _, c := range str {
		switch string(c) {
		case openS:
			r = append(r, tCell{kind: open})
		case treesS:
			r = append(r, tCell{kind: trees})
		case lumberyardS:
			r = append(r, tCell{kind: lumberyard})
		default:
			log.Fatalf("unknown symbol %s", string(c))
		}
	}
	return r
}

func getData(filename string) tField {
	field := tField{}
	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		if len(scanner.Text()) == 0 {
			continue
		}

		field = append(field, getRow(scanner.Text()))
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return field
}

func printField(f tField) {
	for y := 0; y < len(f); y++ {
		var sb strings.Builder
		for x := 0; x < len(f[y]); x++ {
			switch f[y][x].kind {
			case open:
				sb.WriteString(openS)
			case trees:
				sb.WriteString(treesS)
			case lumberyard:
				sb.WriteString(lumberyardS)
			}
		}
		fmt.Println(sb.String())
	}
	fmt.Println()
}

func getAdjacentAreas(f tField, x, y int) []tCell {
	areas := []tCell{}
	if y > 0 {
		if x > 0 {
			areas = append(areas, f[y-1][x-1])
		}
		areas = append(areas, f[y-1][x])
		if x < len(f[y])-1 {
			areas = append(areas, f[y-1][x+1])
		}
	}
	if x > 0 {
		areas = append(areas, f[y][x-1])
	}
	if x < len(f[y])-1 {
		areas = append(areas, f[y][x+1])
	}

	if y < len(f[y])-1 {
		if x > 0 {
			areas = append(areas, f[y+1][x-1])
		}
		areas = append(areas, f[y+1][x])
		if x < len(f[y])-1 {
			areas = append(areas, f[y+1][x+1])
		}
	}

	return areas
}

func getOpen(f tField, x, y int) tCell {
	areas := getAdjacentAreas(f, x, y)
	treeCount := 0
	for _, a := range areas {
		if a.kind == trees {
			treeCount++
		}
	}
	if treeCount >= 3 {
		return tCell{kind: trees}
	}
	return tCell{kind: open}
}

func getTrees(f tField, x, y int) tCell {
	areas := getAdjacentAreas(f, x, y)
	lumberyardCount := 0
	for _, a := range areas {
		if a.kind == lumberyard {
			lumberyardCount++
		}
	}
	if lumberyardCount >= 3 {
		return tCell{kind: lumberyard}
	}
	return tCell{kind: trees}
}

func getLumberyard(f tField, x, y int) tCell {
	areas := getAdjacentAreas(f, x, y)
	lumberyardCount := 0
	treesCount := 0
	for _, a := range areas {
		if a.kind == lumberyard {
			lumberyardCount++
		}
		if a.kind == trees {
			treesCount++
		}
	}
	if lumberyardCount >= 1 && treesCount >= 1 {
		return tCell{kind: lumberyard}
	}
	return tCell{kind: open}
}

func doIteration(f tField) tField {
	newF := tField{}
	for y, row := range f {
		newR := []tCell{}
		for x, cell := range row {
			switch cell.kind {
			case open:
				newR = append(newR, getOpen(f, x, y))
			case trees:
				newR = append(newR, getTrees(f, x, y))
			case lumberyard:
				newR = append(newR, getLumberyard(f, x, y))
			}
		}
		newF = append(newF, newR)
	}
	return newF
}

func printResourceValue(f tField) int {
	lumberyardCount := 0
	treesCount := 0
	for _, row := range f {
		for _, cell := range row {
			if cell.kind == lumberyard {
				lumberyardCount++
				continue
			}
			if cell.kind == trees {
				treesCount++
			}
		}
	}
	fmt.Printf("Resource value: %d\n", lumberyardCount*treesCount)
	return lumberyardCount * treesCount
}

func main() {

	//f := getData("input_example.txt")
	f := getData("input.txt")
	printField(f)
	for i := 0; i < 10000; i++ {
		f = doIteration(f)
	}
	var rOld, rNew int

	shift := (1000000000 - 10000) % 28
	fmt.Printf("shift: %d ", shift)
	for j := 0; j < shift; j++ {
		f = doIteration(f)
		rNew = printResourceValue(f)
		fmt.Printf("j: %03d, rDiff: %d ", j, rOld-rNew)
		rOld = rNew
	}
	printField(f)
	printResourceValue(f)

	//part1 - 637550

	//part2 - 201465
}
