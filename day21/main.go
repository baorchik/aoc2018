package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

type tInstruction struct {
	opname string
	op     interface{}
	args   tArgs
}

type tArgs []int
type tRegistry []int64
type tInstructions map[int]tInstruction

func getIpFromString(str string) int {
	log.Println(str)
	var v int
	_, err := fmt.Sscanf(str, "#ip %d", &v)
	if err != nil {
		log.Fatal(err)
	}
	return v
}

func getInstructionFromString(str string) tInstruction {
	log.Println(str)
	var a, b, c int
	var inst string
	_, err := fmt.Sscanf(str, "%s %d %d %d", &inst, &a, &b, &c)
	if err != nil {
		log.Fatal(err)
	}

	opId := -1
	for i, op := range opcodesNames {
		if op == inst {
			opId = i
			break
		}
	}
	if opId == -1 {
		panic(fmt.Sprintf("unmapped instruction %s", inst))
	}

	return tInstruction{
		opname: inst,
		op:     opcodes[opId],
		args:   tArgs{0, a, b, c}}
}

func getData(filename string) (tInstructions, int) {
	instructions := tInstructions{}
	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)

	ok := scanner.Scan()
	if !ok {
		panic("scan error")
	}
	ip := getIpFromString(scanner.Text())
	i := 0
	for scanner.Scan() {
		if len(scanner.Text()) == 0 {
			continue
		}
		instructions[i] = getInstructionFromString(scanner.Text())
		i++
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return instructions, ip
}

func addr(input tArgs, registry tRegistry) {
	registry[input[3]] = registry[input[1]] + registry[input[2]]
}
func addi(input tArgs, registry tRegistry) {
	registry[input[3]] = registry[input[1]] + int64(input[2])
}

func mulr(input tArgs, registry tRegistry) {
	registry[input[3]] = registry[input[1]] * registry[input[2]]
}
func muli(input tArgs, registry tRegistry) {
	registry[input[3]] = registry[input[1]] * int64(input[2])
}

func banr(input tArgs, registry tRegistry) {
	registry[input[3]] = registry[input[1]] & registry[input[2]]
}
func bani(input tArgs, registry tRegistry) {
	registry[input[3]] = registry[input[1]] & int64(input[2])
}

func borr(input tArgs, registry tRegistry) {
	registry[input[3]] = registry[input[1]] | registry[input[2]]
}
func bori(input tArgs, registry tRegistry) {
	registry[input[3]] = registry[input[1]] | int64(input[2])
}

func setr(input tArgs, registry tRegistry) {
	registry[input[3]] = registry[input[1]]
}
func seti(input tArgs, registry tRegistry) {
	registry[input[3]] = int64(input[1])
}

func gtir(input tArgs, registry tRegistry) {
	if int64(input[1]) > registry[input[2]] {
		registry[input[3]] = 1
	} else {
		registry[input[3]] = 0
	}
}
func gtri(input tArgs, registry tRegistry) {
	if registry[input[1]] > int64(input[2]) {
		registry[input[3]] = 1
	} else {
		registry[input[3]] = 0
	}
}
func gtrr(input tArgs, registry tRegistry) {
	if registry[input[1]] > registry[input[2]] {
		registry[input[3]] = 1
	} else {
		registry[input[3]] = 0
	}
}

func eqir(input tArgs, registry tRegistry) {
	if int64(input[1]) == registry[input[2]] {
		registry[input[3]] = 1
	} else {
		registry[input[3]] = 0
	}
}
func eqri(input tArgs, registry tRegistry) {
	if registry[input[1]] == int64(input[2]) {
		registry[input[3]] = 1
	} else {
		registry[input[3]] = 0
	}
}
func eqrr(input tArgs, registry tRegistry) {
	if registry[input[1]] == registry[input[2]] {
		registry[input[3]] = 1
	} else {
		registry[input[3]] = 0
	}
}

var opcodes = []interface{}{
	addr,
	addi,
	mulr,
	muli,
	banr,
	bani,
	borr,
	bori,
	setr,
	seti,
	gtir,
	gtri,
	gtrr,
	eqir,
	eqri,
	eqrr}

var opcodesNames = []string{
	"addr",
	"addi",
	"mulr",
	"muli",
	"banr",
	"bani",
	"borr",
	"bori",
	"setr",
	"seti",
	"gtir",
	"gtri",
	"gtrr",
	"eqir",
	"eqri",
	"eqrr"}

func wrapperGtir25643(registry tRegistry, eqValues map[int64]bool) bool {
	for {
		if 256 <= registry[4] {
			registry[3] = 0
			registry[1] = 256
			for registry[1] <= registry[4] {
				registry[3]++
				registry[1] = (registry[3] + 1) * 256
			}

			registry[1] = 1
			registry[4] = registry[3]

			registry[3] = registry[4] & int64(255)
			registry[2] = (((registry[2] + registry[3]) & int64(16777215)) * int64(65899)) & int64(16777215)
		} else {
			registry[3] = 1
			registry[5] = 28

			if registry[2] == registry[0] {
				registry[3] = 1
				registry[5] = 30
				return true
			}

			lenBefore := len(eqValues)
			eqValues[registry[2]] = true
			fmt.Printf("len: %d, val: %d\n", len(eqValues), registry[2])
			if lenBefore == len(eqValues) {
				fmt.Println("stop!")
				return true
			}

			registry[4] = registry[2] | int64(65536)

			registry[2] = 6718165
			registry[3] = registry[4] & int64(255)
			registry[2] = registry[2] + registry[3]
			registry[2] = registry[2] & int64(16777215)
			registry[2] = registry[2] * int64(65899)
			registry[2] = registry[2] & int64(16777215)
		}
	}
	return false
}

func doIteration(instructions tInstructions, ip int, registry tRegistry, eqValues map[int64]bool, uniqueCodes map[int]bool) bool {
	pointer := int(registry[ip])
	instr, ok := instructions[pointer]
	if !ok {
		fmt.Println("stop!")
		return true
	}

	if pointer == 13 {
		fmt.Println("====================================")
		return wrapperGtir25643(registry, eqValues)
	}

	// if pointer == 28 {
	// 	lenBefore := len(eqValues)
	// 	eqValues[registry[2]] = true
	// 	fmt.Printf("len: %d, val: %d\n", len(eqValues), registry[2])
	// 	if lenBefore == len(eqValues) {
	// 		fmt.Println("stop!")
	// 		return true
	// 	}
	// }
	if !uniqueCodes[pointer] {
		uniqueCodes[pointer] = true
		fmt.Printf("Add code %d\n", pointer)
	}

	// fmt.Printf("ip=%02d %03v ", pointer, registry)
	// fmt.Printf("%s %02d %02d %02d ", instr.opname, instr.args[1], instr.args[2], instr.args[3])
	instr.op.(func(tArgs, tRegistry))(instr.args, registry)
	// fmt.Printf("%03v\n", registry)
	registry[ip]++
	return false
}

func equal(a, b []int) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}

func doExecuteInstructions(instructions tInstructions, ip int) {
	//registry := tRegistry{30842, 0, 0, 0, 0, 0}
	registry := tRegistry{0, 0, 0, 0, 0, 0}

	eqValues := map[int64]bool{}
	uniqueCodes := map[int]bool{}
	var stop bool
	for j := 0; j <= 50; j++ {
		stop = doIteration(instructions, ip, registry, eqValues, uniqueCodes)
		if stop {
			fmt.Println("stop!")
			fmt.Printf("j: %d\n", j)
			break
		}
	}
}

func main() {
	instructions, ip := getData("input.txt")
	fmt.Printf("%v\n", instructions)
	fmt.Printf("ip: %d\n", ip)
	doExecuteInstructions(instructions, ip)

	//part1 - 30842
	// part2 - 9617088  too low
	// part2 - 41512 too low !!!
	// part2 - 4851 too low !!!!!

	// part2 - 10748062

}
