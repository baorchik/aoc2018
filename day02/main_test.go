package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetStrMap_1(t *testing.T) {
	expected := map[byte]int{'a': 2}
	res := getStrMap("aa")
	assert.Equal(t, expected, res)
}

func TestGetStrMap_2(t *testing.T) {
	expected := map[byte]int{'a': 2, 'b': 3, 'c': 4}
	res := getStrMap("aabbccbcc")
	assert.Equal(t, expected, res)
}

func TestGetCountMap_1(t *testing.T) {
	input := map[byte]int{'a': 2, 'b': 3, 'c': 4}
	expected := map[int]int{2: 1, 3: 1, 4: 1}
	res := getCountMap(input)
	assert.Equal(t, expected, res)
}
