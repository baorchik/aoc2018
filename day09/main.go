package main

import (
	"bufio"
	"container/list"
	"fmt"
	"log"
	"os"
)

type dataLine struct {
	players int
	marbles int
	scores  int
}

type elem struct {
	value int
	prev  *elem
	next  *elem
}

func getTestData(filename string) []dataLine {
	data := []dataLine{}
	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		line := dataLine{}
		log.Println(scanner.Text())
		_, err := fmt.Sscanf(scanner.Text(),
			"%d players; last marble is worth %d points: high score is %d",
			&line.players, &line.marbles, &line.scores)
		if err != nil {
			log.Fatal(err)
		}
		data = append(data, line)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return data
}

func getInputData(filename string) []dataLine {
	data := []dataLine{}
	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		line := dataLine{}
		log.Println(scanner.Text())
		_, err := fmt.Sscanf(scanner.Text(),
			"%d players; last marble is worth %d points",
			&line.players, &line.marbles)
		if err != nil {
			log.Fatal(err)
		}
		data = append(data, line)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return data
}

func insertElement(c *list.List, current *list.Element, new int) *list.Element {
	index := current.Next()
	if index == nil {
		index = c.Front()
	}
	return c.InsertAfter(new, index)
}

func removeElement(c *list.List, current *list.Element) (int, *list.Element) {
	index := current.Prev()
	for i := 0; i < 6; i++ {
		if index == nil {
			//printList(c)
			index = c.Back()
		}
		index = index.Prev()
	}
	current = index.Next()
	if current == nil {
		current = c.Front()
	}

	c.Remove(index)
	v, ok := index.Value.(int)
	if !ok {
		log.Fatal("error reading value!")
	}
	return v, current
}

func printList(l *list.List) {
	for e := l.Front(); e != nil; e = e.Next() {
		fmt.Printf("%+v ", e.Value)
	}
	fmt.Println()
}

func game(data dataLine) map[int]int {
	circle := list.New()
	current := circle.PushBack(0)
	scores := map[int]int{}
	player := 0
	for m := 1; m <= data.marbles; m++ {
		//printList(circle)
		player++
		if player > data.players {
			player = 1
		}
		if m%23 == 0 {
			s := 0
			s, current = removeElement(circle, current)
			scores[player] += m + s
			//log.Printf("score! p: %d, m: %d, r: %d", player, m, s)
			//log.Printf("scores: %+v", scores)
			continue
		}
		current = insertElement(circle, current, m)
		//log.Printf("p:%d", player)
	}
	return scores
}

func main() {
	//data := getTestData("input_example.txt")
	data := getInputData("input.txt")
	log.Printf("data: %+v", data)

	for _, d := range data {
		log.Printf("run for data: %+v", d)
		scores := game(d)

		var maxP, maxV int
		for p, v := range scores {
			if v > maxV {
				maxP = p
				maxV = v
			}
		}
		log.Printf("winner: player %d scores %d", maxP, maxV)
		if d.scores > 0 {
			if d.scores == maxV {
				log.Println("Result is correct!")
			} else {
				log.Fatalf("Expected result: %d, actual: %d", d.scores, maxV)
			}
		}
	}
	// part1 - 410375
	// part2 - 3314195047
}
