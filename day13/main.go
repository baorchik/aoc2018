package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

const (
	nothing int = iota + 1
	horizontal
	vertical
	curveNe
	curveNw
	intersection
	up
	down
	left
	right
	crash
)

const (
	nothingS      = " "
	horizontalS   = "-"
	verticalS     = "|"
	curveNeS      = "/"
	curveNwS      = "\\"
	intersectionS = "+"
	upS           = "^"
	downS         = "v"
	leftS         = "<"
	rightS        = ">"
	crashS        = "X"
)

const (
	moveL int = iota + 1
	moveS
	moveR
)

type cell struct {
	cType    int
	cartID   int
	cartType int
}

type row []cell
type field []row

type cart struct {
	x         int
	y         int
	move      int
	iscrashed bool
}

type carts map[int]cart

func getRowFromStr(str string, crts carts) row {

	r := row{}
	for _, c := range str {
		cartId := len(crts) + 1
		switch string(c) {
		case nothingS:
			r = append(r, cell{cType: nothing})
		case horizontalS:
			r = append(r, cell{cType: horizontal})
		case verticalS:
			r = append(r, cell{cType: vertical})
		case curveNeS:
			r = append(r, cell{cType: curveNe})
		case curveNwS:
			r = append(r, cell{cType: curveNw})
		case intersectionS:
			r = append(r, cell{cType: intersection})
		case upS:
			r = append(r, cell{
				cType:    vertical,
				cartID:   cartId,
				cartType: up})
			crts[cartId] = cart{move: moveL}
		case downS:
			r = append(r, cell{
				cType:    vertical,
				cartID:   cartId,
				cartType: down})
			crts[cartId] = cart{move: moveL}
		case leftS:
			r = append(r, cell{
				cType:    horizontal,
				cartID:   cartId,
				cartType: left})
			crts[cartId] = cart{move: moveL}
		case rightS:
			r = append(r, cell{
				cType:    horizontal,
				cartID:   cartId,
				cartType: right})
			crts[cartId] = cart{move: moveL}
		default:
			log.Fatalf("unknown symbol %s", string(c))
		}
	}
	return r
}

// Doesn't work if initial cart stays on intersection
func getfield(filename string) (field, carts) {
	carts := carts{}
	fld := []row{}
	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)

	var r row
	for scanner.Scan() {
		log.Println(scanner.Text())
		r = getRowFromStr(scanner.Text(), carts)
		fld = append(fld, r)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return fld, carts
}

func printCellType(t int) {
	switch t {
	case nothing:
		fmt.Print(nothingS)
	case horizontal:
		fmt.Print(horizontalS)
	case vertical:
		fmt.Print(verticalS)
	case curveNe:
		fmt.Print(curveNeS)
	case curveNw:
		fmt.Print(curveNwS)
	case intersection:
		fmt.Print(intersectionS)
	}
}

func printCartType(c int) {
	switch c {
	case up:
		fmt.Print(upS)
	case down:
		fmt.Print(downS)
	case left:
		fmt.Print(leftS)
	case right:
		fmt.Print(rightS)
	case crash:
		fmt.Print(crashS)
	}
}

func printField(f field) {
	for _, r := range f {
		for _, c := range r {
			if c.cartID == 0 {
				printCellType(c.cType)
			} else {
				printCartType(c.cartType)
			}
		}
		fmt.Println()
	}
	fmt.Println("***************")
}

func getNextMove(currentMove int) int {
	if currentMove == moveL {
		return moveS
	}
	if currentMove == moveS {
		return moveR
	}
	if currentMove == moveR {
		return moveL
	}
	log.Fatalln("wrong move!")
	return 0
}

func moveCart(f field, crt cart) cart {
	i := crt.x
	j := crt.y
	cartcell := f[i][j]

	f[i][j] = cell{
		cType:    cartcell.cType,
		cartID:   0,
		cartType: 0}

	var newi, newj, newCartType int
	newCartType = cartcell.cartType
	switch cartcell.cType {
	case intersection:
		cartMove := crt.move
		crt.move = getNextMove(cartMove)
		if crt.move == 0 {
			crt.move = moveL
		}
		switch cartcell.cartType {
		case up:
			switch cartMove {
			case moveL:
				newi = i
				newj = j - 1
				newCartType = left
			case moveR:
				newi = i
				newj = j + 1
				newCartType = right
			case moveS:
				newi = i - 1
				newj = j
			}
		case down:
			switch cartMove {
			case moveL:
				newi = i
				newj = j + 1
				newCartType = right
			case moveR:
				newi = i
				newj = j - 1
				newCartType = left
			case moveS:
				newi = i + 1
				newj = j
			}
		case left:
			switch cartMove {
			case moveL:
				newi = i + 1
				newj = j
				newCartType = down
			case moveR:
				newi = i - 1
				newj = j
				newCartType = up
			case moveS:
				newi = i
				newj = j - 1
			}
		case right:
			switch cartMove {
			case moveL:
				newi = i - 1
				newj = j
				newCartType = up
			case moveR:
				newi = i + 1
				newj = j
				newCartType = down
			case moveS:
				newi = i
				newj = j + 1
			}
		}
	case horizontal:
		newi = i
		switch cartcell.cartType {
		case up:
			log.Fatalln("Wrong move!")
		case down:
			log.Fatalln("Wrong move!")
		case left:
			newj = j - 1
		case right:
			newj = j + 1
		}
	case vertical:
		newj = j
		switch cartcell.cartType {
		case up:
			newi = i - 1
		case down:
			newi = i + 1
		case left:
			log.Fatalln("Wrong move!")
		case right:
			log.Fatalln("Wrong move!")
		}
	case curveNe:
		switch cartcell.cartType {
		case up:
			newi = i
			newj = j + 1
			newCartType = right
		case down:
			newi = i
			newj = j - 1
			newCartType = left
		case left:
			newi = i + 1
			newj = j
			newCartType = down
		case right:
			newi = i - 1
			newj = j
			newCartType = up
		}
	case curveNw:
		switch cartcell.cartType {
		case up:
			newi = i
			newj = j - 1
			newCartType = left
		case down:
			newi = i
			newj = j + 1
			newCartType = right
		case left:
			newi = i - 1
			newj = j
			newCartType = up
		case right:
			newi = i + 1
			newj = j
			newCartType = down
		}
	}

	if f[newi][newj].cartID != 0 {
		f[newi][newj].cartType = crash
		log.Println("crash!!!!")
		crt.iscrashed = true
	} else {
		f[newi][newj].cartID = cartcell.cartID
		f[newi][newj].cartType = newCartType
	}
	crt.x = newi
	crt.y = newj

	return crt
}

func moveCart2(f field, crt cart, carts carts) cart {
	i := crt.x
	j := crt.y
	cartcell := f[i][j]

	f[i][j] = cell{
		cType:    cartcell.cType,
		cartID:   0,
		cartType: 0}

	var newi, newj, newCartType int
	newCartType = cartcell.cartType
	switch cartcell.cType {
	case intersection:
		cartMove := crt.move
		crt.move = getNextMove(cartMove)
		if crt.move == 0 {
			crt.move = moveL
		}
		switch cartcell.cartType {
		case up:
			switch cartMove {
			case moveL:
				newi = i
				newj = j - 1
				newCartType = left
			case moveR:
				newi = i
				newj = j + 1
				newCartType = right
			case moveS:
				newi = i - 1
				newj = j
			}
		case down:
			switch cartMove {
			case moveL:
				newi = i
				newj = j + 1
				newCartType = right
			case moveR:
				newi = i
				newj = j - 1
				newCartType = left
			case moveS:
				newi = i + 1
				newj = j
			}
		case left:
			switch cartMove {
			case moveL:
				newi = i + 1
				newj = j
				newCartType = down
			case moveR:
				newi = i - 1
				newj = j
				newCartType = up
			case moveS:
				newi = i
				newj = j - 1
			}
		case right:
			switch cartMove {
			case moveL:
				newi = i - 1
				newj = j
				newCartType = up
			case moveR:
				newi = i + 1
				newj = j
				newCartType = down
			case moveS:
				newi = i
				newj = j + 1
			}
		}
	case horizontal:
		newi = i
		switch cartcell.cartType {
		case up:
			log.Fatalln("Wrong move!")
		case down:
			log.Fatalln("Wrong move!")
		case left:
			newj = j - 1
		case right:
			newj = j + 1
		}
	case vertical:
		newj = j
		switch cartcell.cartType {
		case up:
			newi = i - 1
		case down:
			newi = i + 1
		case left:
			log.Fatalln("Wrong move!")
		case right:
			log.Fatalln("Wrong move!")
		}
	case curveNe:
		switch cartcell.cartType {
		case up:
			newi = i
			newj = j + 1
			newCartType = right
		case down:
			newi = i
			newj = j - 1
			newCartType = left
		case left:
			newi = i + 1
			newj = j
			newCartType = down
		case right:
			newi = i - 1
			newj = j
			newCartType = up
		}
	case curveNw:
		switch cartcell.cartType {
		case up:
			newi = i
			newj = j - 1
			newCartType = left
		case down:
			newi = i
			newj = j + 1
			newCartType = right
		case left:
			newi = i - 1
			newj = j
			newCartType = up
		case right:
			newi = i + 1
			newj = j
			newCartType = down
		}
	}

	if f[newi][newj].cartID != 0 {
		log.Println("crash!!!!")
		crt.iscrashed = true
		tcart := carts[f[newi][newj].cartID]
		tcart.iscrashed = true
		carts[f[newi][newj].cartID] = tcart

		f[newi][newj].cartID = 0
		f[newi][newj].cartType = 0
	} else {
		f[newi][newj].cartID = cartcell.cartID
		f[newi][newj].cartType = newCartType
	}
	crt.x = newi
	crt.y = newj

	return crt
}

func initCartsCoord(f field, carts carts) {
	for i := 0; i < len(f); i++ {
		for j := 0; j < len(f[i]); j++ {
			if f[i][j].cartID != 0 {
				carts[f[i][j].cartID] = cart{
					move: carts[f[i][j].cartID].move,
					x:    i,
					y:    j}
			}
		}
	}
}

func doMove(f field, carts carts) bool {
	for id, cart := range carts {
		carts[id] = moveCart(f, cart)
		if carts[id].iscrashed {
			log.Printf("crash X,y: %d,%d", carts[id].y, carts[id].x)
			return true
		}
	}
	return false
}

func doMoveUntilLast(f field, carts carts) bool {
	for id, cart := range carts {
		carts[id] = moveCart2(f, cart, carts)
		if carts[id].iscrashed {
			log.Printf("crash X,y: %d,%d", carts[id].y, carts[id].x)
			delete(carts, id)
			for k, v := range carts {
				if v.iscrashed {
					delete(carts, k)
				}
			}
			log.Printf("carts %v", carts)
		}
	}
	if len(carts) == 1 {
		for _, v := range carts {
			log.Printf("Last cart X,y: %d,%d", v.y, v.x)
		}
		return true
	}
	return false
}

func main() {
	var crts carts
	field, crts := getfield("input.txt")
	//field, crts := getfield("input_example.txt")
	//field, crts := getfield("input_example2.txt")
	initCartsCoord(field, crts)
	//printField(field)

	/*
		for i := 0; i < 200; i++ {
			stop := doMove(field, crts)
			//printField(field)
			if stop {
				break
			}
		}
	*/

	//part1 crash X,y: 103,85

	for i := 0; i < 50000; i++ {
		stop := doMoveUntilLast(field, crts)
		//printField(field)
		if stop {
			break
		}
	}

	// part2
	// 88,64
}
