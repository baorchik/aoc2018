package main

import (
	"bufio"
	"log"
	"os"
)

func getBoxIds() []string {
	boxIds := []string{}

	f, err := os.Open("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		boxIds = append(boxIds, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return boxIds
}

func getStrMap(str string) map[byte]int {
	countMap := map[byte]int{}
	for i := 0; i < len(str); i++ {
		char := str[i]
		if countMap[char] >= 1 {
			countMap[char]++
		} else {
			countMap[char] = 1
		}
	}
	log.Printf("countMap: %q", countMap)
	return countMap
}

func getCountMap(strMap map[byte]int) map[int]int {
	countMap := map[int]int{}

	for _, v := range strMap {
		if countMap[v] > 0 {
			countMap[v]++
		} else {
			countMap[v] = 1
		}
	}

	return countMap
}

func getCandidateBoxes(boxIds []string) []string {
	checksum2chars := 0
	checksum3chars := 0
	candidateBoxesUniqueMap := map[string]bool{}

	for _, id := range boxIds {
		strMap := getStrMap(id)
		countMap := getCountMap(strMap)
		if countMap[2] > 0 {
			checksum2chars++
			log.Printf("2s: %v, 3s: %v", checksum2chars, checksum3chars)
			candidateBoxesUniqueMap[id] = true
		}
		if countMap[3] > 0 {
			checksum3chars++
			log.Printf("2s: %v, 3s: %v", checksum2chars, checksum3chars)
			candidateBoxesUniqueMap[id] = true
		}
	}
	log.Printf("2s: %v, 3s: %v", checksum2chars, checksum3chars)
	log.Printf("checksum: %v should be 6370", checksum2chars*checksum3chars)

	candidateBoxes := []string{}
	for k := range candidateBoxesUniqueMap {
		candidateBoxes = append(candidateBoxes, k)
	}
	return candidateBoxes
}

func IsBoxDiffBy1ch(boxId1 string, boxId2 string) bool {
	if len(boxId1) != len(boxId2) {
		log.Fatalf("box1 length is %v, box2 lenght is %v", len(boxId1), len(boxId2))
	}
	diffcount := 0
	for i := 0; i < len(boxId1); i++ {
		if boxId1[i] != boxId2[i] {
			diffcount++
		}
	}
	return diffcount == 1
}

func getCandidateBoxesClosePairs(boxIds []string) map[string]string {
	boxPairs := map[string]string{}
	for i := 0; i < len(boxIds)-1; i++ {
		box1 := boxIds[i]
		for j := i + 1; j < len(boxIds); j++ {
			box2 := boxIds[j]

			if IsBoxDiffBy1ch(box1, box2) {
				if len(boxPairs[box1]) > 0 {
					log.Fatalf("box %v alerady have a pair %v. Can't add a new pair %v", box1, boxPairs[box1], box2)
				}
				boxPairs[box1] = box2
			}
		}
	}
	return boxPairs
}

func getCommonCharsFromFirstBoxPair(boxPairs map[string]string) string {
	var commonChars string
	for k, v := range boxPairs {
		for i := 0; i < len(k); i++ {
			if k[i] == v[i] {
				commonChars += string(k[i])
			}
		}
	}
	return commonChars
}

func main() {
	boxIds := getBoxIds()
	candidateBoxes := getCandidateBoxes(boxIds)
	log.Printf("candidateBoxes: %v", candidateBoxes)
	pairs := getCandidateBoxesClosePairs(candidateBoxes)
	log.Printf("box pairs: %v", pairs)

	commonChars := getCommonCharsFromFirstBoxPair(pairs)
	log.Printf("common chars: %v. Right answer: rmyxgdlihczskunpfijqcebtv", commonChars)
}
