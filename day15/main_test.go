package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestKnownPath0(t *testing.T) {
	input := []string{
		"####",
		"#.G#",
		"#..#",
		"####"}
	f := stringsToField(input)

	res := getPath(f,
		point{x: 1, y: 1},
		point{x: 2, y: 1})

	fmt.Printf("path: %+v\n", res)
	assert.Equal(t, 1, len(res))

	assert.Equal(t, 1, res[0].x)
	assert.Equal(t, 1, res[0].y)
}

func TestKnownPath1(t *testing.T) {
	input := []string{
		"####",
		"#..#",
		"#.G#",
		"####"}
	f := stringsToField(input)

	res := getPath(f,
		point{x: 1, y: 1},
		point{x: 2, y: 2})

	fmt.Printf("path: %+v\n", res)
	assert.Equal(t, 2, len(res))

	assert.Equal(t, 2, res[0].x)
	assert.Equal(t, 1, res[0].y)

	assert.Equal(t, 1, res[1].x)
	assert.Equal(t, 1, res[1].y)
}

func TestKnownPath2(t *testing.T) {
	input := []string{
		"#####",
		"#.#.#",
		"#...#",
		"#..G#",
		"#####"}
	f := stringsToField(input)

	res := getPath(f,
		point{x: 1, y: 1},
		point{x: 2, y: 2})

	fmt.Printf("path: %+v\n", res)
	assert.Equal(t, 2, len(res))

	assert.Equal(t, 1, res[0].x)
	assert.Equal(t, 2, res[0].y)

	assert.Equal(t, 1, res[1].x)
	assert.Equal(t, 1, res[1].y)
}

func TestKnownPath3(t *testing.T) {
	input := []string{
		"#####",
		"#.#.#",
		"#.#.#",
		"#..G#",
		"#####"}
	f := stringsToField(input)

	res := getPath(f,
		point{x: 1, y: 1},
		point{x: 3, y: 3})

	fmt.Printf("path: %+v\n", res)
	assert.Equal(t, 4, len(res))

	assert.Equal(t, 2, res[0].x)
	assert.Equal(t, 3, res[0].y)

	assert.Equal(t, 1, res[1].x)
	assert.Equal(t, 3, res[1].y)

	assert.Equal(t, 1, res[2].x)
	assert.Equal(t, 2, res[2].y)

	assert.Equal(t, 1, res[3].x)
	assert.Equal(t, 1, res[3].y)
}

func TestKnownPath4(t *testing.T) {
	input := []string{
		"####",
		"#..#",
		"#.G#",
		"####"}
	f := stringsToField(input)

	res := getPath(f,
		point{x: 1, y: 1},
		point{x: 2, y: 2})

	fmt.Printf("path: %+v\n", res)
	assert.Equal(t, 2, len(res))

	assert.Equal(t, 2, res[0].x)
	assert.Equal(t, 1, res[0].y)

	assert.Equal(t, 1, res[1].x)
	assert.Equal(t, 1, res[1].y)
}

func fEquals(t *testing.T, fExpected, f field) {
	for y := 0; y < len(f); y++ {
		for x := 0; x < len(f[y]); x++ {
			assert.Equal(t, fExpected[y][x].cType, f[y][x].cType)
			if fExpected[y][x].u != nil {
				assert.Equal(t, (*fExpected[y][x].u).uType, (*f[y][x].u).uType, fmt.Sprintf("a: %+v", (*f[y][x].u)))
				assert.Equal(t, (*fExpected[y][x].u).p.x, (*f[y][x].u).p.x, fmt.Sprintf("a: %+v", (*f[y][x].u)))
				assert.Equal(t, (*fExpected[y][x].u).p.y, (*f[y][x].u).p.y, fmt.Sprintf("a: %+v", (*f[y][x].u)))
				assert.False(t, (*f[y][x].u).isDead, fmt.Sprintf("a: %+v", (*f[y][x].u)))
			}
		}
	}
}

func TestKnownGame1(t *testing.T) {
	input := []string{
		"#######",
		"#.G...#",
		"#...EG#",
		"#.#.#G#",
		"#..G#E#",
		"#.....#",
		"#######"}
	f := stringsToField(input)

	res := []string{
		"#######",
		"#..G..#",
		"#...EG#",
		"#.#G#G#",
		"#...#E#",
		"#.....#",
		"#######"}
	fExpected := stringsToField(res)

	// Act
	doIteration(f)
	printField(f)

	// Assert
	fEquals(t, fExpected, f)
	unts := getUnitsInOrder(f)

	hps := []int{200, 197, 197, 200, 197, 197}
	assert.Equal(t, len(hps), len(unts))
	for i := 0; i < len(hps); i++ {
		assert.Equal(t, hps[i], (*unts[i]).hp, fmt.Sprintf("u: %+v", (*unts[i])))
	}
}
func TestKnownGame2(t *testing.T) {
	input := []string{
		"#######",
		"#.G...#",
		"#...EG#",
		"#.#.#G#",
		"#..G#E#",
		"#.....#",
		"#######"}
	f := stringsToField(input)

	res23 := []string{
		"#######",
		"#...G.#",
		"#..G.G#",
		"#.#.#G#",
		"#...#E#",
		"#.....#",
		"#######"}
	fExpected := stringsToField(res23)

	// Act
	for i := 1; i <= 23; i++ {
		doIteration(f)
	}

	// Assert
	fEquals(t, fExpected, f)
	unts := getUnitsInOrder(f)

	hps := []int{200, 200, 131, 131, 131}
	assert.Equal(t, len(hps), len(unts))
	for i := 0; i < len(hps); i++ {
		assert.Equal(t, hps[i], (*unts[i]).hp, fmt.Sprintf("u: %+v", (*unts[i])))
	}
}
func TestKnownGame3(t *testing.T) {
	input := []string{
		"#######",
		"#.G...#",
		"#...EG#",
		"#.#.#G#",
		"#..G#E#",
		"#.....#",
		"#######"}
	f := stringsToField(input)

	res28 := []string{
		"#######",
		"#G....#",
		"#.G...#",
		"#.#.#G#",
		"#...#E#",
		"#....G#",
		"#######"}
	fExpected := stringsToField(res28)

	// Act
	for i := 1; i <= 28; i++ {
		doIteration(f)
	}

	// Assert
	fEquals(t, fExpected, f)
	unts := getUnitsInOrder(f)

	hps := []int{200, 131, 116, 113, 200}
	assert.Equal(t, len(hps), len(unts))
	for i := 0; i < len(hps); i++ {
		assert.Equal(t, hps[i], (*unts[i]).hp, fmt.Sprintf("u: %+v", (*unts[i])))
	}
}
func TestKnownGame4(t *testing.T) {
	input := []string{
		"#######",
		"#.G...#",
		"#...EG#",
		"#.#.#G#",
		"#..G#E#",
		"#.....#",
		"#######"}
	f := stringsToField(input)

	res47 := []string{
		"#######",
		"#G....#",
		"#.G...#",
		"#.#.#G#",
		"#...#.#",
		"#....G#",
		"#######"}
	fExpected := stringsToField(res47)

	// Act
	iteration := runFight(f)

	// Assert
	assert.Equal(t, 47, iteration)
	fEquals(t, fExpected, f)
	unts := getUnitsInOrder(f)

	hps := []int{200, 131, 59, 200}
	assert.Equal(t, len(hps), len(unts))
	for i := 0; i < len(hps); i++ {
		assert.Equal(t, hps[i], (*unts[i]).hp, fmt.Sprintf("u: %+v", (*unts[i])))
	}
}
func TestKnownGame5(t *testing.T) {
	input := []string{
		"#######",
		"#G..#E#",
		"#E#E.E#",
		"#G.##.#",
		"#...#E#",
		"#...E.#",
		"#######"}
	f := stringsToField(input)
	printField(f)

	res37 := []string{
		"#######",
		"#...#E#",
		"#E#...#",
		"#.E##.#",
		"#E..#E#",
		"#.....#",
		"#######"}
	fExpected := stringsToField(res37)

	// Act
	iteration := runFight(f)
	printField(f)

	// Assert
	assert.Equal(t, 37, iteration)
	fEquals(t, fExpected, f)
	unts := getUnitsInOrder(f)
	assert.Equal(t, 982, hpSum(unts))

	hps := []int{200, 197, 185, 200, 200}
	assert.Equal(t, len(hps), len(unts))
	for i := 0; i < len(hps); i++ {
		assert.Equal(t, hps[i], (*unts[i]).hp, fmt.Sprintf("u: %+v", (*unts[i])))
	}
}
func TestKnownGame6(t *testing.T) {
	input := []string{
		"#######",
		"#E..EG#",
		"#.#G.E#",
		"#E.##E#",
		"#G..#.#",
		"#..E#.#",
		"#######"}
	f := stringsToField(input)

	res46 := []string{
		"#######",
		"#.E.E.#",
		"#.#E..#",
		"#E.##.#",
		"#.E.#.#",
		"#...#.#",
		"#######"}
	fExpected := stringsToField(res46)

	// Act
	iteration := runFight(f)

	// Assert
	assert.Equal(t, 46, iteration)
	fEquals(t, fExpected, f)
	unts := getUnitsInOrder(f)

	assert.Equal(t, 859, hpSum(unts))
	hps := []int{164, 197, 200, 98, 200}
	assert.Equal(t, len(hps), len(unts))
	for i := 0; i < len(hps); i++ {
		assert.Equal(t, hps[i], (*unts[i]).hp, fmt.Sprintf("u: %+v", (*unts[i])))
	}
}
func TestKnownGame7(t *testing.T) {
	input := []string{
		"#######",
		"#E.G#.#",
		"#.#G..#",
		"#G.#.G#",
		"#G..#.#",
		"#...E.#",
		"#######"}
	f := stringsToField(input)

	res35 := []string{
		"#######",
		"#G.G#.#",
		"#.#G..#",
		"#..#..#",
		"#...#G#",
		"#...G.#",
		"#######"}
	fExpected := stringsToField(res35)

	// Act
	iteration := runFight(f)

	// Assert
	assert.Equal(t, 35, iteration)
	fEquals(t, fExpected, f)
	unts := getUnitsInOrder(f)
	assert.Equal(t, 793, hpSum(unts))

	hps := []int{200, 98, 200, 95, 200}
	assert.Equal(t, len(hps), len(unts))
	for i := 0; i < len(hps); i++ {
		assert.Equal(t, hps[i], (*unts[i]).hp, fmt.Sprintf("u: %+v", (*unts[i])))
	}
}
func TestKnownGame8(t *testing.T) {
	input := []string{
		"#######",
		"#.E...#",
		"#.#..G#",
		"#.###.#",
		"#E#G#G#",
		"#...#G#",
		"#######"}
	f := stringsToField(input)

	res54 := []string{
		"#######",
		"#.....#",
		"#.#G..#",
		"#.###.#",
		"#.#.#.#",
		"#G.G#G#",
		"#######"}
	fExpected := stringsToField(res54)

	// Act
	iteration := runFight(f)

	// Assert
	assert.Equal(t, 54, iteration)
	fEquals(t, fExpected, f)
	unts := getUnitsInOrder(f)
	assert.Equal(t, 536, hpSum(unts))

	hps := []int{200, 98, 38, 200}
	assert.Equal(t, len(hps), len(unts))
	for i := 0; i < len(hps); i++ {
		assert.Equal(t, hps[i], (*unts[i]).hp, fmt.Sprintf("u: %+v", (*unts[i])))
	}
}
func TestKnownGame9(t *testing.T) {
	input := []string{
		"#########",
		"#G......#",
		"#.E.#...#",
		"#..##..G#",
		"#...##..#",
		"#...#...#",
		"#.G...G.#",
		"#.....G.#",
		"#########"}
	f := stringsToField(input)

	res20 := []string{
		"#########",
		"#.G.....#",
		"#G.G#...#",
		"#.G##...#",
		"#...##..#",
		"#.G.#...#",
		"#.......#",
		"#.......#",
		"#########"}
	fExpected := stringsToField(res20)

	// Act
	iteration := runFight(f)

	// Assert
	assert.Equal(t, 20, iteration)
	fEquals(t, fExpected, f)

	unts := getUnitsInOrder(f)
	assert.Equal(t, 937, hpSum(unts))

	hps := []int{137, 200, 200, 200, 200}
	assert.Equal(t, len(hps), len(unts))
	for i := 0; i < len(hps); i++ {
		assert.Equal(t, hps[i], (*unts[i]).hp, fmt.Sprintf("u: %+v", (*unts[i])))
	}
}
func TestKnownGame10(t *testing.T) {
	input := []string{
		"#########",
		"#G..G..G#",
		"#.......#",
		"#.......#",
		"#G..E..G#",
		"#.......#",
		"#.......#",
		"#G..G..G#",
		"#########"}
	f := stringsToField(input)

	res := []string{
		"#########",
		"#.G...G.#",
		"#...G...#",
		"#...E..G#",
		"#.G.....#",
		"#.......#",
		"#G..G..G#",
		"#.......#",
		"#########"}
	fExpected := stringsToField(res)

	// Act
	doIteration(f)

	// Assert
	fEquals(t, fExpected, f)

	res = []string{
		"#########",
		"#..G.G..#",
		"#...G...#",
		"#.G.E.G.#",
		"#.......#",
		"#G..G..G#",
		"#.......#",
		"#.......#",
		"#########"}
	fExpected = stringsToField(res)

	// Act
	doIteration(f)

	// Assert
	fEquals(t, fExpected, f)

	res = []string{
		"#########",
		"#.......#",
		"#..GGG..#",
		"#..GEG..#",
		"#G..G...#",
		"#......G#",
		"#.......#",
		"#.......#",
		"#########"}
	fExpected = stringsToField(res)

	// Act
	doIteration(f)

	// Assert
	fEquals(t, fExpected, f)
}
func TestKnownGame11(t *testing.T) {
	input := []string{
		"######",
		"#.G..#",
		"#.EG.#",
		"#.G..#",
		"######"}
	f := stringsToField(input)
	(*f[1][2].u).hp = 200
	(*f[2][2].u).hp = 10
	(*f[2][3].u).hp = 10
	(*f[3][2].u).hp = 10

	res := []string{
		"######",
		"#.G..#",
		"#.EG.#",
		"#.G..#",
		"######"}
	fExpected := stringsToField(res)

	// Act
	doIteration(f)
	print(f)

	// Assert
	fEquals(t, fExpected, f)

	unts := getUnitsInOrder(f)

	hps := []int{200, 1, 7, 10}
	assert.Equal(t, len(hps), len(unts))
	for i := 0; i < len(hps); i++ {
		assert.Equal(t, hps[i], (*unts[i]).hp, fmt.Sprintf("u: %+v", (*unts[i])))
	}
}
func TestKnownGame12(t *testing.T) {
	input := []string{
		"######",
		"#.G..#",
		"#GE.#",
		"#.G..#",
		"######"}
	f := stringsToField(input)
	(*f[1][2].u).hp = 200
	(*f[2][1].u).hp = 10
	(*f[2][2].u).hp = 10
	(*f[3][2].u).hp = 10

	res := []string{
		"######",
		"#.G..#",
		"#GE.#",
		"#.G..#",
		"######"}
	fExpected := stringsToField(res)

	// Act
	doIteration(f)

	// Assert
	fEquals(t, fExpected, f)

	unts := getUnitsInOrder(f)

	hps := []int{200, 7, 1, 10}
	assert.Equal(t, len(hps), len(unts))
	for i := 0; i < len(hps); i++ {
		assert.Equal(t, hps[i], (*unts[i]).hp, fmt.Sprintf("u: %+v", (*unts[i])))
	}
}

func TestKnownGame13(t *testing.T) {
	input := []string{
		"######",
		"#.G..#",
		"#GE.#",
		"#.G..#",
		"######"}
	f := stringsToField(input)
	(*f[1][2].u).hp = 10
	(*f[2][1].u).hp = 10
	(*f[2][2].u).hp = 10
	(*f[3][2].u).hp = 10

	res := []string{
		"######",
		"#.G..#",
		"#GE.#",
		"#.G..#",
		"######"}
	fExpected := stringsToField(res)

	// Act
	doIteration(f)

	// Assert
	fEquals(t, fExpected, f)

	unts := getUnitsInOrder(f)

	hps := []int{7, 10, 1, 10}
	assert.Equal(t, len(hps), len(unts))
	for i := 0; i < len(hps); i++ {
		assert.Equal(t, hps[i], (*unts[i]).hp, fmt.Sprintf("u: %+v", (*unts[i])))
	}
}

func TestKnownGame14(t *testing.T) {
	input := []string{
		"#######",
		"#E..G.#",
		"#...#.#",
		"#.G.#G#",
		"#######"}
	f := stringsToField(input)

	res := []string{
		"#######",
		"#.EG..#",
		"#.G.#.#",
		"#...#G#",
		"#######"}
	fExpected := stringsToField(res)

	// Act
	doIteration(f)

	printField(f)

	// Assert
	fEquals(t, fExpected, f)
}

func TestKnownGame15(t *testing.T) {
	input := []string{
		"####",
		"#GE#",
		"#E##",
		"#GE#",
		"####"}
	f := stringsToField(input)
	(*f[1][1].u).hp = 1
	(*f[2][1].u).hp = 1

	res := []string{
		"####",
		"#E.#",
		"#.##",
		"#GE#",
		"####"}
	fExpected := stringsToField(res)

	// Act
	doIteration(f)
	doIteration(f)

	printField(f)

	// Assert
	fEquals(t, fExpected, f)
}

func TestUnitIsActive(t *testing.T) {
	u := &unit{}
	assert.True(t, u.isActive())
	u.isDead = true
	assert.False(t, u.isActive())
	u = nil
	assert.False(t, u.isActive())
}
